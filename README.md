# Hiikey
### Post and discover private events 

Features
======
* Dicover do it yourself (DIY) events or events held at private spaces. 
* Hosts, easily create, invite guests, and post your event 

Video Explanation Demo: 

https://www.youtube.com/watch?v=_PGjoRvWTNI&t=190s

When hosts post their event, their invitees are sent a text message with the event link in case they don't have the app downloaded. 
This is done using Twilio in the backend on Hiikey's server. 
Twilio requires that the phone number include the country code, but problem is that numbers retreived through users' contact list didn't always have the country. 
To solve this I wrote an algorithm that checks if the country code is included, then appends the country if it doesn't. 

The function below fetches the users' contact info, then parses it by name and phone number. 
After, it checks if the phone number has the country code prefix and appends if it doesn't. 
Finally, a tuple is created to keep track of which number belongs to who and is sorted alpabetically. 

```swift 
    func loadContacts(){
        let toFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        let request = CNContactFetchRequest(keysToFetch: toFetch as [CNKeyDescriptor])
        let store = CNContactStore()
        
        var unSortedNames = [String]()
        var unSortedNumbers = [String]()
        
        do{
            try store.enumerateContacts(with: request) {
                contact, stop in
                
                if contact.phoneNumbers.count != 0{
                    unSortedNames.append(contact.givenName + " " + contact.familyName)
                    self.isInvited.append(false)
                    
                    //get phone number
                    let phoneString = (contact.phoneNumbers.first?.value)?.value(forKey: "digits") as! String
                    
                    //get country code
                    let countryCodeString = (contact.phoneNumbers.first?.value)?.value(forKey: "countryCode") as! String
                    
                    //country code identifier
                    let charset = CharacterSet(charactersIn: "+")
                    
                    //determine if "phoneNumber" has country code prefix
                    if phoneString.rangeOfCharacter(from: charset) != nil{
                        //number already has country code, append to array
                        unSortedNumbers.append(phoneString)
                        
                    } else {
                        //loop through country codes to find one that matches returned country code from current user's contact book
                        for (country, countryCode) in self.countryCodes{
                            //create new contact with country prefix
                            if countryCodeString == country{
                                unSortedNumbers.append("+\(countryCode)\(phoneString)")
                            }
                        }
                    }
                }
            }
            
            //combine the two for sorting the names alphebetically
            let sortInfo = zip(unSortedNames, unSortedNumbers).sorted(by: {<})
            
            //seperate the sorted data into their respective arrays
            self.name = sortInfo.map{$0.0}
            self.phoneNumber = sortInfo.map{$0.1}
            
            self.tableJaunt.reloadData()
            
        } catch let err{
            print(err)
        }
    }
```
