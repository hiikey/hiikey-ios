//
//  EditProfile_TableViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 9/17/16.
//  Copyright © 2016 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse
import Kingfisher
import Alamofire
import TwitterKit
import DigitsKit
import FacebookCore
import FacebookLogin

class EditProfile_TableViewController: UITableViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //event
    var codeObjectId: String!
    var codeTitle: String!
    var codeStartDate: Date!
    var codeEndDate: Date!
    var codeLocation: CLLocation!
    var codeAddress: String!
    var codeImageURL: String!
    var codeIsHostSubscribed: Bool!
    var codeChatUserIds: String!
    var codeMessages: String!
    var codeCreatedAt: String!
    var codeCode: String!
    
    @IBOutlet weak var facebookButton: UIButton!
    var facebookId = ""
    
    @IBOutlet weak var twitterButton: UIButton!
    var twitterUsername = ""
    
    //user info
    @IBOutlet weak var uploadImageLabel: UILabel!
    @IBOutlet weak var userImage: UIButton!
    
    //Display name
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var usernameJaunt: UILabel!
    @IBOutlet weak var phoneNumberField: UITextField!
    
    var userNameString: String!
    var passwordString: String!
    var emailString: String!
    
    let imagePicker = UIImagePickerController()
    
    //tableview
    @IBOutlet var tableJaunt: UITableView!
    
    var activityIndicator: UIActivityIndicatorView!
    
    //event info 
    var startDateString: String!
    var startDateJaunt: Date!
    var endDateJaunt: Date!
    var descriptionJaunt: String!
    var titleJaunt: String!
    var addressJaunt: String!
    var locationJaunt: PFGeoPoint!
    var eventImageJaunt: PFFile!
    var rawEventImage: UIImage!
    
    var codeJaunt: String!
    
    //invites
    var numberToSend = [String]()
    var nameToSend = [String]()
    
    var uploadedImage: PFFile!
    
    var isJoiningEvent = false
    
    var newEventId: String!
    
    lazy var signoutButton: UIButton = {
        let button =  UIButton(frame: CGRect(x: 0, y: 0, width: 125, height: 40))
        button.setTitle("Sign out", for: .normal)
        button.setTitleColor(UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        button.addTarget(self, action: #selector(EditProfile_TableViewController.navigationAction(_:)), for: UIControlEvents.touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let proPic = UIImageJPEGRepresentation(UIImage(named: "appy")!, 0.5)
        uploadedImage = PFFile(name: "defaultProfile_ios.jpeg", data: proPic!)
        
        imagePicker.delegate = self
        
        self.createNavigation()
        
        //check to see if new user
        if PFUser.current() != nil{
            self.navigationItem.titleView = self.signoutButton
            
            let query = PFQuery(className:"_User")
            query.getObjectInBackground(withId: (PFUser.current()?.objectId)!) {
                (object: PFObject?, error: Error?) -> Void in
                if error == nil && object != nil {
                    let imageFile: PFFile = object!["Profile"] as! PFFile
                    
                    self.userImage.kf.setBackgroundImage(with: URL(string: imageFile.url!), for: .normal)
                    self.userImage.layer.cornerRadius = self.userImage.frame.width / 2
                    self.userImage.clipsToBounds = true
                    self.uploadImageLabel.isHidden = false
                    self.userName.text = object?["DisplayName"] as? String
                    self.usernameJaunt.text = "\(object!["username"] as! String)"
                    
                    if object?["facebook"] != nil{
                        if object?["facebook"] as! String != ""{
                            self.facebookButton.setBackgroundImage(UIImage(named: "facebook-filled"), for: .normal)
                            self.facebookId = object?["facebook"] as! String
                        }
                    }
                    
                    if object?["twitter"] != nil{
                        if object?["twitter"] as! String != ""{
                            self.twitterButton.setBackgroundImage(UIImage(named: "twitter-filled"), for: .normal)
                            self.twitterUsername = object?["twitter"] as! String
                        }
                    }
                    
                    if object?["phoneNumber"] != nil{
                        self.phoneNumberField.text = (object?["phoneNumber"] as! String)
                    }
                    
                } else {
                    print(error!)
                }
            }
            
        } else {
            self.title = "Profile"
            self.userImage.layer.cornerRadius = self.userImage.frame.width / 2
            self.userImage.clipsToBounds = true
            self.uploadImageLabel.isHidden = false
            self.usernameJaunt.text = "\(userNameString!)"
        }
    }

    @IBAction func picUpload(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func updateButton(_ sender: UIBarButtonItem) {
        
        progressBarDisplayer()
        self.view.isUserInteractionEnabled = false
        
        if PFUser.current() != nil{
            let query = PFQuery(className:"_User")
            query.getObjectInBackground(withId: (PFUser.current()?.objectId)!) {
                (object: PFObject?, error: Error?) -> Void in
                if error == nil && object != nil {
                    object?["DisplayName"] = self.userName.text
                    object?["phoneNumber"] = self.phoneNumberField.text
                    object?["facebook"] = self.facebookId
                    object?["twitter"] = self.twitterUsername
                    object?.saveEventually()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "main") as UIViewController
                    self.present(controller, animated: true, completion: nil)
                    
                } else {
                    print(error!)
                }
            }
            
        } else {
            var displayName = userName.text!
            //make user's name their username if they decide not to provide their name
            if userName.text! == ""{
                displayName = userNameString
            }
            
            newUser(displayName: displayName, username: userNameString, password: passwordString, email: emailString, imageFile: uploadedImage)
        }
    }
    
    func newUser( displayName: String, username: String,
                  password: String, email: String, imageFile: PFFile ){
            let user = PFUser()
            user.username = username
            user.password = password
            user.email = email
            user["DisplayName"] = displayName
            user["Profile"] = imageFile
            user["phoneNumber"] = phoneNumberField.text
            user["twitter"] = twitterUsername
            user["facebook"] = facebookId
            user.signUpInBackground{ (succeeded: Bool, error: Error?) -> Void in
                if error == nil{
                    //associate current user with device
                    let installation = PFInstallation.current()
                    installation?["user"] = PFUser.current()
                    installation?["userId"] = PFUser.current()?.objectId
                    installation?.saveEventually()
                    
                    if self.isJoiningEvent{
                        self.findEvent(self.codeJaunt)
                        
                    } else {
                        self.createEvent(displayName)
                    }
                }
            }
    }
    
    func createEvent(_ displayName: String){
        
        let username = PFUser.current()!.username!
        
        var closedTitle = ""
        let titleBits = titleJaunt!.split{$0 == " "}.map(String.init)
        for object in titleBits{
            closedTitle = closedTitle.lowercased() + object.lowercased()
        }
        
        let code = "\(closedTitle)-\(username)"
        codeJaunt = code 
        
        //check to see if image was uploaded
        let newEvent = PFObject(className: "Event")
        newEvent["startTime"] = startDateJaunt
        newEvent["endTime"] = endDateJaunt
        newEvent["description"] = descriptionJaunt
        newEvent["title"] = titleJaunt
        newEvent["address"] = addressJaunt
        newEvent["location"] = locationJaunt
        newEvent["userId"] = PFUser.current()?.objectId
        if eventImageJaunt != nil{
            newEvent["eventImage"] = eventImageJaunt
        }

        newEvent["code"] = code
        newEvent["isRemoved"] = false
        newEvent["isSubscribed"] = true
        newEvent["invites"] = numberToSend
        newEvent.saveInBackground {
            (success: Bool, error: Error?) -> Void in
            if (success) {
                //notify guest list users
                let nameToSend = "\(displayName)(\(username))"
                
                self.newEventId = newEvent.objectId

                self.sendInvites(nameToSend, code: code)
                
            } else {
                //show alert that tells the user that the event failed to create
                self.view.isUserInteractionEnabled = true
                self.activityIndicator.removeFromSuperview()
                self.showAlert(title: "Problem with Creation", message: "Check internet connection and try again.")
            }
        }
    }
    
    func findEvent(_ code: String){
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("endTime", greaterThan: Date())
        eventQuery.whereKey("code", equalTo: code)
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                
                //add variable jaunts
                self.codeObjectId = object?.objectId
                self.codeTitle = object?["title"] as! String
                self.codeStartDate = object?["startTime"] as! Date
                self.codeEndDate = object?["endTime"] as! Date
                
                let pfLocation = object?["location"] as! PFGeoPoint
                let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                
                self.codeLocation = clLocation
                self.codeAddress = object?["address"] as! String
                
                if object?["eventImage"] != nil{
                    let eventImage: PFFile = object?["eventImage"] as! PFFile
                    self.codeImageURL = eventImage.url
                }

                self.codeIsHostSubscribed = object?["isSubscribed"] as! Bool
                self.codeChatUserIds = object?["userId"] as! String
                self.codeMessages = object?["description"] as! String
                self.codeCode = object?["code"] as! String
                
                let createdAt = DateFormatter.localizedString(from: (object?.createdAt!)!, dateStyle: .short, timeStyle: .short)
                
                self.codeCreatedAt = createdAt
                
                self.goToEventPage()
                
            } else {
                let prompt = UIAlertController(title: "Couldn't Find The Event", message: "The event date may have passed or the host removed it from Hiikey.", preferredStyle: .alert)
                
                prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                
                self.present(prompt, animated: true, completion: nil)
            }
        }
    }
    
    func goToEventPage(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "EventPage") as! EventPageController
        //event info
        initialViewController.objectId = self.codeObjectId
        initialViewController.eventTitle = self.codeTitle
        initialViewController.rawStartDate = self.codeStartDate
        initialViewController.rawEndDate = self.codeEndDate
        initialViewController.eventLocation = self.codeLocation
        initialViewController.eventAddress = self.codeAddress
        if self.codeImageURL != nil{
            initialViewController.eventImageURL = self.codeImageURL
        }
        initialViewController.isHostSubscribed = self.codeIsHostSubscribed
        initialViewController.eventCode = self.codeJaunt
        
        //messages
        initialViewController.chatUserIds.append(self.codeChatUserIds)
        initialViewController.messages.append(self.codeMessages)
        initialViewController.createdAt.append(self.codeCreatedAt)
        initialViewController.isLink = true
        initialViewController.userNumber = phoneNumberField.text! 
        
        self.present(initialViewController, animated: true, completion: nil)
    }
    
    func sendInvites(_ displayName: String, code: String){
        
        let url = URL(string: "https://hiikey.herokuapp.com/invites")
        
        let paramerters = [
            "phoneNumbers": numberToSend,
            "names": nameToSend,
            "hostname": displayName,
            "eventName": titleJaunt,
            "code": code,
            "eventDate": startDateString
            
            ] as [String : Any]
        
        Alamofire.request(url!, method: .get, parameters: paramerters ).responseJSON{ response in
            print(response.request!)
            print(response.response!)
            print(response.result)
            
            //look at response and determine if was a success
            
            let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "shareView") as! ShareViewController
            //event info
            initialViewController.eventTitle = self.titleJaunt!
            initialViewController.eventId = self.newEventId!
            
            if self.rawEventImage != nil{
                initialViewController.eventImage = self.rawEventImage
                initialViewController.isImage = true
            }
            
            self.present(initialViewController, animated: true, completion: nil)
        }
    }
    
    //Image picker functions
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        dismiss(animated: true, completion: nil)

        userImage.setBackgroundImage(chosenImage, for: .normal)
        tableJaunt.reloadData()
        
        let proPic = UIImageJPEGRepresentation(chosenImage, 0.5)
        uploadedImage = PFFile(name: "profile_ios.jpeg", data: proPic!)
        uploadedImage?.saveInBackground {
            (success: Bool, error: Error?) -> Void in
            if (success) {
                if PFUser.current() != nil{
                    let query = PFQuery(className:"_User")
                    query.getObjectInBackground(withId: (PFUser.current()?.objectId)!) {
                        (object: PFObject?, error: Error?) -> Void in
                        if error == nil && object != nil {
                            if self.uploadedImage != nil{
                                object!["Profile"] = self.uploadedImage
                            }
                            object?.saveEventually()
                            
                        } else {
                            print(error!)
                        }
                    }
                }
            }
        }
    }
    
    func progressBarDisplayer() {
        
        let messageFrame = UIView(frame: CGRect(x: view.frame.midX, y: view.frame.midY - 25 , width: 50, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        messageFrame.addSubview(activityIndicator)
        view.addSubview(messageFrame)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    @IBAction func verifyPhoneAction(_ sender: UITextField) {
        Digits.sharedInstance().logOut()
        let digits = Digits.sharedInstance()
        digits.authenticate { (session, error) in
            // Inspect session/error objects
            if error == nil && session != nil{
                self.phoneNumberField.text = session?.phoneNumber
                
            } else {
                self.phoneNumberField.text = ""
            }
        }
    }
    
    @IBAction func facebookAction(_ sender: UIButton) {
        let loginManager = LoginManager()
        if self.facebookId == ""{
            //login user and retreive id if the access token has expired/nil
            loginManager.logIn(readPermissions: [.publicProfile], viewController: self) { loginResult in
                switch loginResult {
                case .failed(let error):
                    print(error)
                    
                case .cancelled:
                    print("User cancelled login.")
                    
                case .success( _,  _, let accessToken):
                    self.facebookButton.setBackgroundImage(UIImage(named: "facebook-filled"), for: .normal)
                    self.facebookId = accessToken.userId!
                }
            }
            
        } else {
            loginManager.logOut()
            self.facebookButton.setBackgroundImage(UIImage(named: "facebook"), for: .normal)
            self.facebookId = ""
        }
    }
    
    @IBAction func twitterAction(_ sender: UIButton) {
        if self.twitterUsername == ""{
            Twitter.sharedInstance().logIn { session, error in
                if (session != nil) {
                    self.twitterUsername = (session?.userName)!
                    self.twitterButton.setBackgroundImage(UIImage(named: "twitter-filled"), for: .normal)
                }
            }
            
        } else {
            self.twitterUsername = ""
            self.twitterButton.setBackgroundImage(UIImage(named: "twitter"), for: .normal)
        }
    }
        
    func createNavigation(){
        let doneItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(EditProfile_TableViewController.updateButton(_:)))
        self.navigationItem.rightBarButtonItem  = doneItem
    }

    @objc func navigationAction(_ sender: UIButton){
        PFUser.logOut()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "welcome") as UIViewController
        self.present(controller, animated: true, completion: nil)
    }

    func showAlert(title: String, message: String){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(prompt, animated: true, completion: nil)
    }
}
