//
//  ActivityViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 1/5/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse
import Kingfisher 
import Alamofire
import SnapKit

class ActivityViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableJaunt: UITableView!
    
    var refresh: UIRefreshControl!
    
    var areMentionsSelected = true
    
    @IBOutlet weak var noRequestsTitle: UILabel!
    @IBOutlet weak var noRequestsLabel: UILabel!
    
    @IBOutlet weak var clearButton: UIBarButtonItem!
    
    lazy var activitySegment: UISegmentedControl = {
        let segmentedControl = UISegmentedControl()
        segmentedControl.insertSegment(withTitle: "Mentions", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "Requests", at: 1, animated: false)
        segmentedControl.tintColor = UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0)
        return segmentedControl
        
    }()
    
    var codeObjectId: String!
    var codeTitle: String!
    var codeStartDate: Date!
    var codeEndDate: Date!
    var codeLocation: CLLocation!
    var codeAddress: String!
    var codeImageURL: String!
    var codeIsHostSubscribed: Bool!
    var codeChatUserIds: String!
    var codeMessages: String!
    var codeCreatedAt: String!
    var codeCode: String!
    var codeInvites = [String]()
    
    //request variables
    var requestUserId = [String]()
    var requestEventId = [String]()
    var requestEventTitle = [String]()
    var requestId = [String]()
    
    //users
    var requestUsername = [String]()
    var requestImage = [String]()
    var requestTwitter = [String]()
    var requestFacebook = [String]()
    
    // mention variables
    var mentionUserId = [String]()
    var mentionEventId = [String]()
    var mentionEventTitle = [String]()
    var mentionMessage = [String]()
    var mentionMessageId = [String]()
    var mentionCreatedAt = [String]()
    
    //users
    var mentionUsername = [String]()
    var mentionImage = [String]()
    
    var tappedUsername: String!
    var tappedMessageId: String!
    
    //make sure that users aren't reloaded if there isn't new request data
    var objectCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup tableview
        self.tableJaunt.register(MainTableViewCell.self, forCellReuseIdentifier: "MentionCell")
        self.tableJaunt.register(MainTableViewCell.self, forCellReuseIdentifier: "RequestCell")
        self.tableJaunt.estimatedRowHeight = 50
        self.tableJaunt.rowHeight = UITableViewAutomaticDimension
        
        refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "")
        refresh.addTarget(self, action: #selector(ActivityViewController.loadRequests(_:)), for: UIControlEvents.valueChanged)
        tableJaunt.addSubview(refresh) // not required when using UITableViewController
        
        self.refresh.beginRefreshing()
        loadMentions(self.refresh)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let desti = segue.destination as! EventPageController
        
        //event info
        desti.objectId = codeObjectId
        desti.eventTitle = codeTitle
        desti.rawStartDate = codeStartDate
        desti.rawEndDate = codeEndDate
        desti.eventLocation = codeLocation
        desti.eventAddress = codeAddress
        if codeImageURL != nil{
            desti.eventImageURL = codeImageURL
        }
        
        desti.isHostSubscribed = codeIsHostSubscribed
        desti.eventCode = codeCode
        desti.invites = codeInvites
        
        //messages
        desti.chatUserIds.append(codeChatUserIds)
        desti.messages.append(codeMessages)
        desti.createdAt.append(codeCreatedAt)
        desti.chatIds.append("description")
        
        desti.activityUsername = tappedUsername
        desti.activityMessageId = tappedMessageId
    }
    
    //mark - tableview
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = .white
        let segmentedControl = UISegmentedControl(frame: CGRect(x: 10, y: 0, width: tableView.frame.width - 20, height: 30))
        segmentedControl.insertSegment(withTitle: "Mentions", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "Requests", at: 1, animated: false)
        segmentedControl.tintColor = uicolorFromHex(0xee1848)
        if areMentionsSelected{
            segmentedControl.selectedSegmentIndex = 0
            
        } else {
            segmentedControl.selectedSegmentIndex = 1
            
        }
        
        segmentedControl.addTarget(self, action: #selector(ActivityViewController.activityTypeSelected(_:)), for: .valueChanged)
        v.addSubview(segmentedControl)
        return v
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "yea"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //using username count because activity is loaded first then user info.. .so will crash because index out of range if going by activity count
        
        if areMentionsSelected{
            return mentionUsername.count
            
        } else {
            return requestUsername.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: MainTableViewCell!
        
        if areMentionsSelected{
            cell = tableView.dequeueReusableCell(withIdentifier: "MentionCell", for: indexPath) as! MainTableViewCell
            
            cell.selectionStyle = .none
            
            cell.activityImage.kf.setImage(with: URL(string: mentionImage[indexPath.row])!, placeholder: UIImage(named: "appy"))
            cell.activityMessage.text = "\(self.mentionUsername[indexPath.row]) in \(self.mentionEventTitle[indexPath.row]): \(self.mentionMessage[indexPath.row])"
            cell.activityCreatedAt.text = self.mentionCreatedAt[indexPath.row]
            
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! MainTableViewCell
            
            cell.selectionStyle = .none
            
            cell.activityImage.kf.setImage(with: URL(string: requestImage[indexPath.row])!, placeholder: UIImage(named: "appy"))
            cell.activityMessage.text = "\(self.requestUsername[indexPath.row]) requested access to \(self.requestEventTitle[indexPath.row])"
            cell.activityAccept.addTarget(self, action: #selector(ActivityViewController.acceptAccessButton(_:)), for: .touchUpInside)
            cell.activityAccept.tag = indexPath.row
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //go to event
        if areMentionsSelected{
            tappedUsername = mentionUsername[indexPath.row]
            tappedMessageId = mentionMessageId[indexPath.row]
            findEvent(eventId: mentionEventId[indexPath.row])
            
        } else {
            let prompt = UIAlertController(title: "Verify \(self.requestUsername[indexPath.row])", message: "", preferredStyle: .actionSheet)
            
            if self.requestTwitter[indexPath.row] != "" || self.requestFacebook[indexPath.row] != ""{
                if self.requestTwitter[indexPath.row] != ""{
                    let twitter = UIAlertAction(title: "Via Twitter", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                        let twitterUsername = self.requestTwitter[indexPath.row]
                        
                        UIApplication.tryURL(urls: [
                            "twitter://user?screen_name=\(twitterUsername)", // App
                            "https://www.twitter.com/\(twitterUsername)" // Website if app fails
                            ])
                    }
                    
                    prompt.addAction(twitter)
                }
                
                if self.requestFacebook[indexPath.row] != ""{
                    let facebook = UIAlertAction(title: "Via Facebook", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        
                        let facebookId = self.requestFacebook[indexPath.row]
                        
                        UIApplication.tryURL(urls: [
                            "fb://profile/\(facebookId)", // App
                            "https://www.facebook.com/\(facebookId)" // Website if app fails
                            ])
                    }
                    prompt.addAction(facebook)
                }
            }
            
            prompt.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(prompt, animated: true, completion: nil)
            
            }
            
    }
    
    //mark - mich.
    @objc func acceptAccessButton(_ sender: UIButton){
        
        let requestId = self.requestId[sender.tag]
        self.requestId.remove(at: sender.tag)

        let eventId = self.requestEventId[sender.tag]
        self.requestEventId.remove(at: sender.tag)
        
        let userId = self.requestUserId[sender.tag]
        self.requestUserId.remove(at: sender.tag)
        
        let eventTitle = self.requestEventTitle[sender.tag]
        self.requestEventTitle.remove(at: sender.tag)
        
        self.requestUsername.remove(at: sender.tag)
        self.requestImage.remove(at: sender.tag)
        
        self.tableJaunt.reloadData()
        
        if self.requestEventId.count == 0{
            self.tableJaunt.backgroundColor = UIColor.clear
            self.tableJaunt.separatorStyle = .none
            self.clearButton.isEnabled = false
            self.noRequestsLabel.text = "Your event requests will appear here."
            self.noRequestsLabel.isHidden = false
            self.noRequestsTitle.isHidden = false
            self.tabBarController?.tabBar.items?[3].badgeValue = nil
            
        } else {
            self.tabBarController?.tabBar.items?[3].badgeValue = "\(requestEventId.count)"
            if #available(iOS 10.0, *) {
                self.tabBarController?.tabBar.items?[3].badgeColor = self.uicolorFromHex(0xee1848)
            }
        }
        
        //*****add remove username and image later ******
        
        let query = PFQuery(className:"RSVP")
        query.getObjectInBackground(withId: requestId) {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil && object != nil {
                object?["isConfirmed"] = true
                object?.saveEventually{
                    (success: Bool, error: Error?) -> Void in
                    if (success) {
                        //send notification
                        self.sendNotification(userId, message: "\(PFUser.current()!.username!) accepted your RSVP to \(eventTitle)", eventId: eventId )
                    }
                }
            }
        }
    }

    @IBAction func clearAction(_ sender: UIBarButtonItem) {
        showAlert()
    }
    
    @objc func loadMentions(_ refresh: UIRefreshControl){
        self.clearButton.isEnabled = false
        
        self.mentionMessage.removeAll()
        self.mentionMessageId.removeAll()
        self.mentionEventId.removeAll()
        self.mentionUserId.removeAll()
        self.mentionEventTitle.removeAll()
        self.mentionImage.removeAll()
        self.mentionUsername.removeAll()
        
        let eventQuery = PFQuery(className: "Mention")
        eventQuery.whereKey("mentionId", equalTo: PFUser.current()!.objectId!)
        eventQuery.addDescendingOrder("createdAt")
        eventQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil{
                if let objects = objects as [PFObject]?{
                    //retrieve the objects from the database
                    for object in objects{
                        self.mentionEventId.append(object["eventId"] as! String)
                        self.mentionUserId.append(object["userId"] as! String)
                        self.mentionEventTitle.append(object["eventTitle"] as! String)
                        self.mentionMessage.append(object["message"] as! String)
                        self.mentionMessageId.append(object["messageId"] as! String)
                        
                        let rawCreatedAt = object.createdAt
                        let createdAt = DateFormatter.localizedString(from: rawCreatedAt!, dateStyle: .short, timeStyle: .short)
                        
                        self.mentionCreatedAt.append(createdAt)
                    }
                    //need to load user data somehow
                    
                    if self.areMentionsSelected{
                        if self.mentionMessage.count == 0{
                            self.tableJaunt.backgroundColor = UIColor.clear
                            self.tableJaunt.separatorStyle = .none
                            
                            self.noRequestsLabel.text = "Your event chat mentions will appear here."
                            self.noRequestsLabel.isHidden = false
                            self.noRequestsTitle.isHidden = false
                            self.tableJaunt?.reloadData()
                            
                        } else {
                            self.noRequestsLabel.isHidden = true
                            self.noRequestsTitle.isHidden = true
                            self.tableJaunt.backgroundColor = UIColor.white
                            self.tableJaunt.separatorStyle = .singleLine
                            self.loadMentionUsers()
                        }
                    }
                    
                    refresh.endRefreshing()
                }
            }
        }
    }
    
    func loadMentionUsers(){
        self.mentionImage.removeAll()
        self.mentionUsername.removeAll()
        for user in self.mentionUserId{
            let query = PFQuery(className:"_User")
            query.getObjectInBackground(withId: user) {
                (object: PFObject?, error: Error?) -> Void in
                self.mentionUsername.append(object?["username"] as! String)
                
                let proPic: PFFile = object!["Profile"] as! PFFile
                self.mentionImage.append(proPic.url!)
                self.tableJaunt?.reloadData()
            }
        }
    }
    
    @objc func loadRequests(_ refresh: UIRefreshControl){
        
        self.requestId.removeAll()
        
        self.requestEventId.removeAll()
        self.requestUserId.removeAll()
        self.requestEventTitle.removeAll()
        self.requestImage.removeAll()
        self.requestUsername.removeAll()
        
        self.requestTwitter.removeAll()
        self.requestFacebook.removeAll()
        
        let eventQuery = PFQuery(className: "RSVP")
        eventQuery.whereKey("eventHostId", equalTo: PFUser.current()!.objectId!)
        eventQuery.addDescendingOrder("createdAt")
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.whereKey("isConfirmed", equalTo: false)
        eventQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil{
                if let objects = objects as [PFObject]?{
                    //retrieve the objects from the database
                    for object in objects{
                        self.requestEventId.append(object["eventId"] as! String)
                        self.requestUserId.append(object["userId"] as! String)
                        self.requestEventTitle.append(object["eventTitle"] as! String)
                        self.requestId.append(object.objectId!)
                    }
                    //need to load user data somehow
                    
                        if self.requestEventId.count == 0{
                            self.tableJaunt.backgroundColor = UIColor.clear
                            self.tableJaunt.separatorStyle = .none
                            self.clearButton.isEnabled = false
                            self.noRequestsLabel.text = "Your event requests will appear here."
                            self.noRequestsLabel.isHidden = false
                            self.noRequestsTitle.isHidden = false
                            self.tableJaunt?.reloadData()
                            
                        } else {
                            self.clearButton.isEnabled = true
                            self.noRequestsLabel.isHidden = true
                            self.noRequestsTitle.isHidden = true
                            self.tableJaunt.backgroundColor = UIColor.white
                            self.tableJaunt.separatorStyle = .singleLine
                            self.loadRequestUsrs()
                            
                            self.tabBarController?.tabBar.items?[3].badgeValue = "\(self.requestEventId.count)"
                            if #available(iOS 10.0, *) {
                                self.tabBarController?.tabBar.items?[3].badgeColor = self.uicolorFromHex(0xee1848)
                            }
                        }
                    
                    refresh.endRefreshing()
                }
            }
        }
    }
    
    func loadRequestUsrs(){
        for user in self.requestUserId{
            let query = PFQuery(className:"_User")
            query.getObjectInBackground(withId: user) {
                (object: PFObject?, error: Error?) -> Void in
                self.requestUsername.append(object?["username"] as! String)
                
                let proPic: PFFile = object!["Profile"] as! PFFile
                self.requestImage.append(proPic.url!)
                
                if object!["facebook"] != nil{
                    self.requestFacebook.append(object!["facebook"] as! String)
                    
                } else {
                    self.requestFacebook.append("")
                }
                
                if object!["twitter"] != nil{
                    self.requestTwitter.append(object!["twitter"] as! String)
                    
                } else {
                    self.requestTwitter.append("")
                }
                
                self.tableJaunt?.reloadData()
            }
        }
        self.tableJaunt?.reloadData()
    }
    
    @objc func activityTypeSelected(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0{
            clearButton.isEnabled = false
            refresh.addTarget(self, action: #selector(ActivityViewController.loadMentions(_:)), for: UIControlEvents.valueChanged)
            loadMentions(self.refresh)

            areMentionsSelected = true
            
        } else {
            refresh.addTarget(self, action: #selector(ActivityViewController.loadRequests(_:)), for: UIControlEvents.valueChanged)
            loadRequests(self.refresh)

            areMentionsSelected = false
        }
    }
    
    func sendNotification(_ user: String, message: String, eventId: String){
        let url = URL(string: "https://hiikey.herokuapp.com/notifications")
        
        Alamofire.request(url!, method: .get, parameters: ["userId":user, "message": message, "eventId": eventId]).responseJSON{ response in
            print(response.request!)
            print(response.response!)
            print(response.result)
        }
    }
    
    func showAlert(){
        let prompt = UIAlertController(title: "Clear all of your Requests?", message: "", preferredStyle: .alert)
        
        let requestJaunt = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            UIAlertAction in
            let requests = self.requestId
            self.requestId.removeAll()
            
            self.requestEventId.removeAll()
            self.requestUserId.removeAll()
            self.requestEventTitle.removeAll()
            self.requestImage.removeAll()
            self.requestUsername.removeAll()
            
            self.tabBarController?.tabBar.items?[3].badgeValue = nil
            
            self.tableJaunt.reloadData()
            let requestQuery = PFQuery(className: "RSVP")
            requestQuery.whereKey("objectId", containedIn: requests)
            requestQuery.whereKey("isRemoved", equalTo: false)
            requestQuery.whereKey("isConfirmed", equalTo: false)
            requestQuery.findObjectsInBackground{
                (objects: [PFObject]?, error: Error?) -> Void in
                if error == nil{
                    if let objects = objects as [PFObject]?{
                        //retrieve the objects from the database
                        for object in objects{
                            object["isRemoved"] = true
                            object.saveEventually()
                        }
                        
                        if self.requestEventId.count == 0{
                            self.tableJaunt.backgroundColor = UIColor.clear
                            self.tableJaunt.separatorStyle = .none
                            self.clearButton.isEnabled = false
                            self.noRequestsLabel.isHidden = false
                            self.noRequestsTitle.isHidden = false
                        }
                        
                        //need to load user data somehow
                        self.tableJaunt.reloadData()
                    }
                }
            }
        }
        
        prompt.addAction(requestJaunt)
        prompt.addAction(UIAlertAction(title: "NO", style: .default, handler: nil))
        
        present(prompt, animated: true, completion: nil)
    }
    
    func findEvent(eventId: String){
        print("checking")
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("objectId", equalTo: eventId)
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                
                //add variable jaunts
                self.codeObjectId = object?.objectId
                self.codeTitle = object?["title"] as! String
                self.codeStartDate = object?["startTime"] as! Date
                self.codeEndDate = object?["endTime"] as! Date
                
                let pfLocation = object?["location"] as! PFGeoPoint
                let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                
                self.codeLocation = clLocation
                self.codeAddress = object?["address"] as! String
                if object?["eventImage"] != nil{
                    let eventImage: PFFile = object?["eventImage"] as! PFFile
                    self.codeImageURL = eventImage.url
                }
                
                self.codeIsHostSubscribed = object?["isSubscribed"] as! Bool
                
                self.codeChatUserIds = object?["userId"] as! String
                self.codeMessages = object?["description"] as! String
                self.codeCode = object?["code"] as! String
                self.codeInvites = object?["invites"] as! Array<String>
                
                let createdAt = DateFormatter.localizedString(from: (object?.createdAt!)!, dateStyle: .short, timeStyle: .short)
                
                self.codeCreatedAt = createdAt
                
                self.performSegue(withIdentifier: "showEvent", sender: self)

            } else {
                self.showAlert(title: "Couldn't find the event", message: "The end date may have passed or the host removed the event.")
            }
        }
    }
    
    func showAlert(title: String, message: String){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(prompt, animated: true, completion: nil)
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if application.canOpenURL(URL(string: url)! ) {
                application.open(URL(string: url)!)
                return
            }
        }
    }
}
