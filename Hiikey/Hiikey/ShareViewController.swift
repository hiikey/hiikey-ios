//
//  ShareViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 2/13/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Social

class ShareViewController: UIViewController {
    
    var eventImage: UIImage!
    var eventId: String!
    var eventTitle: String!
    var isImage = false
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func facebookShareAction(_ sender: UIButton) {
        var objectsToShare = [AnyObject]()
        if isImage{
         
            if let myWebsite = NSURL(string: "https://www.hiikey.com/events?id=\(self.eventId!)"){
                objectsToShare.append(myWebsite)
                objectsToShare.append(eventImage!)
                
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            }
            
        } else{
            if let myWebsite = NSURL(string: "https://www.hiikey.com/events?id=\(self.eventId!)"){
                objectsToShare.append(myWebsite)
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "main") as UIViewController
        self.present(controller, animated: true, completion: nil)
    }
}
