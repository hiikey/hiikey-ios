//
//  SearchViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 3/4/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse
import Kingfisher
import CoreLocation
import SnapKit
import Alamofire

class SearchViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    @IBOutlet weak var tableJaunt: UITableView!
    
    var refresh: UIRefreshControl!
    
    @IBOutlet weak var noEventsTitle: UILabel!
    
    var eventImage = [String]()
    var eventTitle = [String]()
    var eventStartDate = [String]()
    var eventEndDate = [String]()
    var rawEventStartDate = [Date]()
    var rawEventEndDate = [Date]()
    var eventObjectId = [String]()
    var eventUserId = [String]()
    var userLocation: PFGeoPoint!
    var eventDescription = [String]()
    var eventLocation = [CLLocation]()
    var eventAddress = [String]()
    var eventCreatedAt = [String]()
    var eventCode = [String]()
    var isHostSubscribed = [Bool]()
    
    //code 
    //event code variables
    
    var isEventCode = false
    
    var codeObjectId: String!
    var codeTitle: String!
    var codeStartDate: Date!
    var codeEndDate: Date!
    var codeLocation: CLLocation!
    var codeAddress: String!
    var codeImageURL: String!
    var codeIsHostSubscribed: Bool!
    var codeChatUserIds: String!
    var codeMessages: String!
    var codeCreatedAt: String!
    var codeCode: String!
    var codeInvites = [String]()
    
    var userNumber = ""
    
    lazy var locationManager: CLLocationManager! = {
        
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = 16093
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        return manager
    }()
    
    lazy var eventCodeTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont(name: "HelveticaNeue", size: 16)
        textField.placeholder = "Event Code"
        textField.clearButtonMode = .whileEditing
        return textField
    }()
    
    lazy var searchTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont(name: "HelveticaNeue", size: 16)
        textField.placeholder = "Search City"
        textField.clearButtonMode = .whileEditing
        return textField
    }()
    
    lazy var titleButton: UIButton = {
        let button =  UIButton(frame: CGRect(x: 55, y: 0, width: self.view.frame.width - 105, height: 40))
        button.setTitle("", for: .normal)
        button.setTitleColor(UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        button.addTarget(self, action: #selector(SearchViewController.titleAction(_:)), for: UIControlEvents.touchUpInside)
        button.contentHorizontalAlignment = .center
        
        return button
    }()
    
    //index
    var selectedIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = titleButton
        
        self.tableJaunt.register(MainTableViewCell.self, forCellReuseIdentifier: "EventCell")
        self.tableJaunt.estimatedRowHeight = 50
        self.tableJaunt.rowHeight = UITableViewAutomaticDimension
        
        locationManager.startUpdatingLocation()
        
        refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "")
        refresh.addTarget(self, action: #selector(SearchViewController.loadEvents(_:)), for: UIControlEvents.valueChanged)
        tableJaunt.addSubview(refresh)
        
        refresh.beginRefreshing()
        
        if PFUser.current() != nil{
            let query = PFQuery(className:"_User")
            query.getObjectInBackground(withId: PFUser.current()!.objectId!) {
                (object: PFObject?, error: Error?) -> Void in
                
                if object?["phoneNumber"] != nil{
                    self.userNumber = object?["phoneNumber"] as! String
                }
            }
            
        } else {
            let exitItem = UIBarButtonItem(title: "Exit", style: .plain, target: self, action: #selector(SearchViewController.exitAction(_ :)))
            exitItem.tintColor = uicolorFromHex(0xee1848)
            self.navigationItem.leftBarButtonItem = exitItem
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let desti = segue.destination as! EventPageController
        
        if segue.identifier == "showEventPage"{
            if isEventCode{
                desti.objectId = codeObjectId
                desti.eventTitle = codeTitle
                desti.rawStartDate = codeStartDate
                desti.rawEndDate = codeEndDate
                desti.eventLocation = codeLocation
                desti.eventAddress = codeAddress
                if codeImageURL != nil{
                    desti.eventImageURL = codeImageURL
                }
                
                desti.isHostSubscribed = codeIsHostSubscribed
                desti.eventCode = codeCode
                desti.invites = codeInvites
                
                //messages
                desti.chatUserIds.append(codeChatUserIds)
                desti.messages.append(codeMessages)
                desti.createdAt.append(codeCreatedAt)
                desti.chatIds.append("description")
                
            } else {
                //event info
                desti.objectId = eventObjectId[selectedIndex]
                desti.eventTitle = eventTitle[selectedIndex]
                desti.rawStartDate = rawEventStartDate[selectedIndex]
                desti.rawEndDate = rawEventEndDate[selectedIndex]
                desti.eventLocation = eventLocation[selectedIndex]
                desti.eventCode = eventCode[selectedIndex]
                desti.eventAddress = eventAddress[selectedIndex]
                if eventImage[selectedIndex] != ""{
                    desti.eventImageURL = eventImage[selectedIndex]
                }
                
                desti.isHostSubscribed = isHostSubscribed[selectedIndex]
                //messages
                desti.chatUserIds.append(eventUserId[selectedIndex])
                desti.messages.append(eventDescription[selectedIndex])
                desti.createdAt.append(eventCreatedAt[selectedIndex])
                desti.chatIds.append("description")
            }
            
            desti.userNumber = userNumber
        }
    }
    
    // Mark - Location
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        self.titleButton.setTitle("Search City", for: .normal)
        self.refresh.endRefreshing()
        
        //do alert saying couldn't find user location... and prompt to search city
        searchCity(title: "Location Error", message: "\n")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locationCity(locations[0])
        
        self.userLocation = PFGeoPoint(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        
        self.refresh.beginRefreshing()
        
        loadEvents(self.refresh)
    }
    
    func locationCity(_ location: CLLocation){
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = (placemarks![0]) as CLPlacemark
                
                self.titleButton.setTitle("\(pm.locality!)", for: .normal)
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func findGeopoint(){
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(searchTextField.text!, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                
                self.titleButton.setTitle("Search City", for: .normal)
                self.refresh.endRefreshing()
                
                //show alert again
                self.searchCity(title: "Location Error", message: "\n")
                
                print("Error", error!)
            }
            if let placemark = placemarks?.first {
                print("worked")
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                self.titleButton.setTitle(self.searchTextField.text!, for: .normal)
                self.userLocation = PFGeoPoint(latitude: coordinates.latitude, longitude: coordinates.longitude)
                self.refreshVariables()
            }
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventObjectId.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! MainTableViewCell
        cell.selectionStyle = .none
        
        cell.eventImage.kf.setImage(with: URL(string: eventImage[indexPath.row])! , placeholder: UIImage(named: "appy"), options: nil, progressBlock: nil, completionHandler: nil)
        
        cell.eventTitle.text = eventTitle[indexPath.row]
        cell.eventDate.text = eventStartDate[indexPath.row]
        cell.eventDescription.text = eventDescription[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //do event access jaunt
        
        self.selectedIndex = indexPath.row
        
        //if current user is event host
        self.performSegue(withIdentifier: "showEventPage", sender: self)
    }
    
    //loadFunctions
    @objc func loadEvents(_ refresh: UIRefreshControl){
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("objectId", notContainedIn: self.eventObjectId)
        eventQuery.whereKey("location", nearGeoPoint: userLocation, withinMiles: 50)
        eventQuery.addAscendingOrder("startTime")
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil{
                if let objects = objects as [PFObject]?{
                    //retrieve the objects from the database
                    for object in objects{
                        let currentDate = Date()
                        let rawEndTime = object["endTime"] as! Date
                        let rawStartTime = object["startTime"] as! Date
                        
                        if currentDate.compare(rawEndTime) == ComparisonResult.orderedAscending {
                            let startDate = DateFormatter.localizedString(from: rawStartTime, dateStyle: .medium, timeStyle: .short)
                            
                            self.eventStartDate.append(startDate)
                            self.rawEventStartDate.append(rawStartTime)
                            
                            let endDate = DateFormatter.localizedString(from: rawEndTime, dateStyle: .medium, timeStyle: .short)
                            self.eventEndDate.append(endDate)
                            self.rawEventEndDate.append(rawEndTime)
                            
                            if object["eventImage"] != nil{
                                let image: PFFile = object["eventImage"] as! PFFile
                                self.eventImage.append(image.url!)
                                
                            } else {
                                self.eventImage.append("")
                            }
                            
                            self.eventTitle.append(object["title"] as! String)
                            self.eventUserId.append(object["userId"] as! String)
                            self.eventDescription.append(object["description"] as! String)
                            self.eventObjectId.append(object.objectId!)
                            self.eventAddress.append(object["address"] as! String)
                            self.eventCode.append(object["code"] as! String)
                            
                            let pfLocation = object["location"] as! PFGeoPoint
                            let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                            self.eventLocation.append(clLocation)
                            
                            let createdAt = DateFormatter.localizedString(from: object.createdAt!, dateStyle: .short, timeStyle: .short)
                            
                            self.eventCreatedAt.append(createdAt)
                            
                            self.isHostSubscribed.append(object["isSubscribed"] as! Bool)
                        }
                    }
                }
                
                if self.eventObjectId.count == 0{
                    self.tableJaunt.backgroundColor = UIColor.clear
                    self.tableJaunt.separatorStyle = .none
                    self.noEventsTitle.isHidden = false
                    
                } else {
                    self.noEventsTitle.isHidden = true
                    self.tableJaunt.backgroundColor = UIColor.white
                    self.tableJaunt.separatorStyle = .singleLine
                }
                
                self.tableJaunt.reloadData()
                refresh.endRefreshing()
            }
        }
    }
    
    //mark - mich
    func refreshVariables(){
        eventStartDate.removeAll()
        rawEventStartDate.removeAll()
        
        eventEndDate.removeAll()
        rawEventEndDate.removeAll()
        
        eventImage.removeAll()
        
        eventTitle.removeAll()
        eventUserId.removeAll()
        eventDescription.removeAll()
        eventObjectId.removeAll()
        eventAddress.removeAll()
        eventCode.removeAll()
        eventLocation.removeAll()
        
        eventCreatedAt.removeAll()
        self.tableJaunt.reloadData()
        
        loadEvents(self.refresh)
    }
    
    @objc func titleAction(_ sender: UIButton){
        searchCity(title: "Where to?", message: "\n")
    }
    
    func showAlert(title: String, description: String, pending: Bool){
        let prompt = UIAlertController(title: title, message: description, preferredStyle: .alert)
        
        prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        
        if !pending{
            let requestJaunt = UIAlertAction(title: "Request Access", style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                let newRequest = PFObject(className: "Request")
                newRequest["eventId"] = self.eventObjectId[self.selectedIndex]
                //change to current user
                newRequest["eventHostId"] = self.eventUserId[self.selectedIndex]
                newRequest["userId"] = PFUser.current()?.objectId
                newRequest["isConfirmed"] = false
                newRequest["isRemoved"] = false
                newRequest["eventTitle"] = self.eventTitle[self.selectedIndex]
                newRequest.saveInBackground {
                    (success: Bool, error: Error?) -> Void in
                    if (success) {
                        // send notification to event host
                        self.sendNotification(self.eventUserId[self.selectedIndex], message: "\(String(describing: PFUser.current()?.username!)) requested access to \(self.eventTitle)")
                    }
                }
            }
            
            prompt.addAction(requestJaunt)
        }
        
        present(prompt, animated: true, completion: nil)
    }
    
    func searchCity(title: String, message: String){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        prompt.view.addSubview(self.searchTextField)
        
        searchTextField.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(prompt.view).offset(25)
            make.left.equalTo(prompt.view).offset(10)
            make.right.equalTo(prompt.view).offset(-10)
            make.bottom.equalTo(prompt.view).offset(-25)
        }
        
        let search = UIAlertAction(title: "Search", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.findGeopoint()
        }
        
        prompt.addAction(search)
        prompt.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        present(prompt, animated: true, completion: nil)
    }
    
    @IBAction func searchEvents(_ sender: UIBarButtonItem) {
        let prompt = UIAlertController(title: "Find Event", message: "", preferredStyle: .alert)
        
        prompt.view.addSubview(self.eventCodeTextField)
        
        eventCodeTextField.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(prompt.view).offset(25)
            make.left.equalTo(prompt.view).offset(10)
            make.right.equalTo(prompt.view).offset(-10)
            make.bottom.equalTo(prompt.view).offset(-25)
        }
        
        let search = UIAlertAction(title: "Search", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.findEvent()
        }
        
        prompt.addAction(search)
        prompt.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))

        present(prompt, animated: true, completion: nil)
    }
    
    @objc func exitAction(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "welcome") as UIViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    func findEvent(){
        
        print("checking")
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("code", equalTo: eventCodeTextField.text!.lowercased())
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                self.isEventCode = true
                
                //add variable jaunts
                self.codeObjectId = object?.objectId
                self.codeTitle = object?["title"] as! String
                self.codeStartDate = object?["startTime"] as! Date
                self.codeEndDate = object?["endTime"] as! Date
                
                let pfLocation = object?["location"] as! PFGeoPoint
                let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                
                self.codeLocation = clLocation
                self.codeAddress = object?["address"] as! String
                if object?["eventImage"] != nil{
                    let eventImage: PFFile = object?["eventImage"] as! PFFile
                    self.codeImageURL = eventImage.url
                }
                
                self.codeIsHostSubscribed = object?["isSubscribed"] as! Bool
                
                self.codeChatUserIds = object?["userId"] as! String
                self.codeMessages = object?["description"] as! String
                self.codeCode = object?["code"] as! String
                self.codeInvites = object?["invites"] as! Array<String>
                
                let createdAt = DateFormatter.localizedString(from: (object?.createdAt!)!, dateStyle: .short, timeStyle: .short)
                
                self.codeCreatedAt = createdAt
                
                self.performSegue(withIdentifier: "showEventPage", sender: self)

            } else {
                self.isEventCode = false
                self.showAlert(title: "Couldn't find the event", message: "The end date may have passed or the host removed the event.")
            }
        }
    }
    
    func showAlert(title: String, message: String){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(prompt, animated: true, completion: nil)
    }
    
    func sendNotification(_ user: String, message: String){
        let url = URL(string: "https://hiikey.herokuapp.com/notifications")
        
        Alamofire.request(url!, method: .get, parameters: ["userId":user, "message": message]).responseJSON{ response in
            print(response.request!)
            print(response.response!)
            print(response.result)
        }
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
