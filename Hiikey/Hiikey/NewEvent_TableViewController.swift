//
//  NewEvent_TableViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 12/1/15.
//  Copyright © 2015 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse
import CoreLocation
import Kingfisher
import Alamofire
import Contacts
import SnapKit

class NewEvent_TableViewController: UITableViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {
    
    var defaults = UserDefaults.standard
    
    var isNewUser = false
    
    var nextViewController: String!
    
     let imagePicker = UIImagePickerController()
    @IBOutlet weak var eventImage: UIButton!
    var uploadedImage: PFFile!
    var rawEventImage: UIImage!
    
    @IBOutlet weak var actionButton: UIBarButtonItem!
    
    //UI labels & Textfields
    @IBOutlet var tableJaunt: UITableView!
    var cellCount = 1
    
    //info
    @IBOutlet weak var titleField: UITextField!
    
    @IBOutlet weak var addressField: UITextField!
    var addressBool = false
    
    @IBOutlet weak var descriptionButton: UIButton!
    
    @IBOutlet weak var startTime: UIButton!
    var starTimeDate: Date!
    var endTimeDate: Date!
    @IBOutlet weak var endTime: UIButton!
    
    @IBOutlet weak var exitButton: UIBarButtonItem!
    var dateType = "start"
    var prompt : UIAlertController!

    var location: PFGeoPoint!
    
    var messageFrame: UIView!
    
    //var dateJaunt
    var titleJaunt: String!
    var  addressJaunt: String!
    var descriptionJaunt: String! 
    var startJaunt: String!
    var endJaunt: String!
    var imageJaunt: String!
    
    //check to see if user is updating
    var eventId: String! 
    var isUpdating = false
    
    // parameter was initially set where address is found when person stops editing..
    //problem was that can't continue if person doesn't edit another field before continueing..  
    //such as date/time field
    var didTouchOutside = false
    
    var rsvpIds = [String]()
    
    //date 
    var dateJaunt: NSDate!
    
    let screenSize: CGRect = UIScreen.main.bounds
    
    //
    var descriptionView: UIView!
    
    lazy var descriptionTextView: UITextView = {
        let label = UITextView()
        label.font = UIFont(name: "Helvetica Neue", size: 17 )
        label.isScrollEnabled = false
        label.text = "description and website links"
        label.textColor = UIColor.lightGray
        label.isScrollEnabled = true
        return label
    }()
    
    lazy var descriptionExit: UIButton = {
        let button = UIButton()
        button.setTitle("Done", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0)
        button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 14)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(NewEvent_TableViewController.exitDescriptionAction), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //to keep track of whether or not user is editing description
        actionButton.tag = 0
        
        if isNewUser{
            let exitItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(NewEvent_TableViewController.exitAction(_:)))
            self.navigationItem.leftBarButtonItem = exitItem
        }
        
        self.title = "New Event"
        
        imagePicker.delegate = self
                
        //is updating event details 
        if isUpdating{
            actionButton.title = "Update"
            self.title = "Edit Event"
            addressBool = true
            titleField.text = titleJaunt
            addressField.text = addressJaunt
            descriptionButton.setTitle(descriptionJaunt, for: .normal)
            descriptionButton.setTitleColor(.black, for: .normal)
            descriptionTextView.text = descriptionJaunt
            startTime.setTitle(startJaunt, for: .normal)
            endTime.setTitle(endJaunt, for: .normal)
            if imageJaunt != nil{
                eventImage.kf.setBackgroundImage(with: URL(string: imageJaunt)!, for: .normal)
            }
            loadRsvpUsers()
        }
        
        descriptionView = UIView(frame: view.bounds)
        descriptionView.addSubview(descriptionExit)
        descriptionView.addSubview(descriptionTextView)
        
        descriptionTextView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(descriptionView).offset(35)
            make.left.equalTo(descriptionView)
            make.right.equalTo(descriptionView)
            make.bottom.equalTo(descriptionView)
        }
        
        descriptionTextView.delegate = self
        
        // Uncomment the following line to preserve selection between presentations
         self.clearsSelectionOnViewWillAppear = false        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContactAccess"{
            let desti = segue.destination as! ContactAccessViewController
            
            var closedTitle = ""
            
            let titleBits = titleField.text?.split{$0 == " "}.map(String.init)
            for object in titleBits!{
                closedTitle = closedTitle.lowercased() + object.lowercased()
            }
            
            desti.startDateJaunt = starTimeDate
            desti.endDateJaunt = endTimeDate
            desti.descriptionJaunt = descriptionTextView.text
            desti.titleJaunt = titleField.text!
            desti.addressJaunt = addressField.text
            desti.locationJaunt = location
            if uploadedImage != nil{
                desti.eventImageJaunt = uploadedImage
            }
            
            desti.startDateString = startTime.titleLabel?.text
            if rawEventImage != nil{
                desti.rawEventImage = rawEventImage
            }
            
        } else {
            let desti =  segue.destination as! GuestListViewController
            desti.startDateJaunt = starTimeDate
            desti.endDateJaunt = endTimeDate
            desti.descriptionJaunt = descriptionTextView.text
            desti.titleJaunt = titleField.text!
            desti.addressJaunt = addressField.text
            desti.locationJaunt = location
            if uploadedImage != nil{
                desti.eventImageJaunt = uploadedImage
            }
            desti.startDateString = startTime.titleLabel?.text
            if rawEventImage != nil{
                desti.rawEventImage = rawEventImage
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 0
            
        } else if section == 1{
            return 1
            
        } else{
            return 5
        }
    }

    //photo methods
    
    //Image picker functions
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        rawEventImage = chosenImage
        
        dismiss(animated: true, completion: nil)
        
        eventImage.setBackgroundImage(chosenImage, for: .normal)
        
        let proPic = UIImageJPEGRepresentation(chosenImage, 0.5)
        uploadedImage = PFFile(name: "eventImage_ios.jpeg", data: proPic!)
        uploadedImage.saveInBackground()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //UIActions
    @IBAction func picUpload(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func addressAction(sender: UITextField) {
        
        didTouchOutside = true
        
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(addressField.text!, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                self.addressField.textColor = UIColor.red
                self.addressField.attributedPlaceholder = NSAttributedString(string:"😕 No location results for: " + self.addressField.text!, attributes:[NSAttributedStringKey.foregroundColor: self.uicolorFromHex(0xee1848)])
                self.addressBool = false
              self.addressField.text = nil 
            }
            
            if let placemark = placemarks?.first {
                self.addressField.textColor = self.uicolorFromHex(0x12a683)
                
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                
                self.location = PFGeoPoint(latitude: coordinates.latitude, longitude: coordinates.longitude)
                
                self.addressBool = true
            }
        })
    }

    @IBAction func nextAction(sender: UIBarButtonItem) {
        if sender.tag == 0{
            if checkUIComponents(){
                if isUpdating{
                    updateEvent()
                    
                } else if !self.defaults.bool(forKey: "showContactAccess"){
                    self.performSegue(withIdentifier: "showContactAccess", sender: self)
                    
                } else {
                    self.performSegue(withIdentifier: "showGuestList", sender: self)
                }
            }
            
        } else {
            sender.tag = 0
            
            self.tableJaunt.isScrollEnabled = true
            
            if isUpdating{
                actionButton.title = "Update"
                self.title = "Edit Event"
                
            } else{
                actionButton.title = "Next"
                self.title = "New Event"
            }
            
            if descriptionTextView.text == " "{
                descriptionButton.setTitle("description and website links", for: .normal)
                descriptionButton.setTitleColor(UIColor.lightGray, for: .normal)
                
            } else {
                descriptionButton.setTitle(descriptionTextView.text, for: .normal)
                descriptionButton.setTitleColor(UIColor.black, for: .normal)
            }
            self.descriptionView.removeFromSuperview()
        }
    }
    
    @IBAction func startTimeAction(_ sender: UIButton) {
        chooseDateAction(type: "start")
        dateType = "start"
    }
    
    @IBAction func endTimeAction(_ sender: UIButton) {
        chooseDateAction(type: "end")
        dateType = "end"
    }
    
    //private functions
    func updateEvent(){
        let query = PFQuery(className:"Event")
        query.getObjectInBackground(withId: eventId) {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil && object != nil {
                if self.uploadedImage != nil{
                    object?["eventImage"] = self.uploadedImage
                }
                
                if self.starTimeDate != nil{
                    object?["startTime"] = self.starTimeDate
                }
                
                if self.endTimeDate != nil{
                    object?["endTime"] = self.endTimeDate
                }
                
                if self.location != nil{
                    object?["location"] = self.location
                }
                
                object?["description"] = self.descriptionTextView.text
                object?["title"] = self.titleField.text
                object?["address"] = self.addressField.text
                
                object?.saveInBackground {
                    (success: Bool, error: Error?) -> Void in
                    if (success) {
                        //notify users of event details change
                        for user in self.rsvpIds{
                            self.sendNotification(user, message: "\(PFUser.current()!.username!) updated the \(self.titleJaunt!) event details")
                        }
                        
                        //show main storyboard
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "main") as UIViewController
                        
                        self.present(controller, animated: true, completion: nil)
                        
                    } else {
                        //show alert that tells the user that the even failed to create
                        self.view.isUserInteractionEnabled = true
                        self.showAlert(title: "Problem with update", message: "Check internet connection and try again.", doAction: false)
                    }
                }
            }
        }
    }
    
    func checkUIComponents() -> Bool{
        var isUIOkay = false
        
        //check to see if the user entered a title
        if titleField.text == ""{
            isUIOkay = false
            titleField.attributedPlaceholder = NSAttributedString(string:"Title required",
                                                                  attributes:[NSAttributedStringKey.foregroundColor: self.uicolorFromHex(0xee1848)])
        } else{
            isUIOkay = true
        }
        
        //if yes, check rest of ish
        if addressField.text == ""{
            isUIOkay = false
            addressField.attributedPlaceholder = NSAttributedString(string:"Address required",
                                                                    attributes:[NSAttributedStringKey.foregroundColor: self.uicolorFromHex(0xee1848)])
        } else if addressBool{
            isUIOkay = true
        } else{
            isUIOkay = false
        }
        
        if starTimeDate == nil{
            isUIOkay = false
            showAlert(title: "START DATE Required", message: "", doAction: false)
            
        } else if endTimeDate == nil {
            isUIOkay = false
            showAlert(title: "END DATE Required", message: "", doAction: false)
            
        } else {
         isUIOkay = true
        }
        
        return isUIOkay
    }
    
    func chooseDateAction(type: String){
        prompt = UIAlertController(title: "Choose \(type)", message: "", preferredStyle: .actionSheet)
        
        //date jaunts
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.dateAndTime
        datePickerView.addTarget(self, action: #selector(NewEvent_TableViewController.datePickerAction(_:)), for: UIControlEvents.valueChanged)
        prompt.view.addSubview(datePickerView)
        
        let datePicker = UIAlertAction(title: "", style: UIAlertActionStyle.default) {
            UIAlertAction in
        }
        
        datePicker.isEnabled = false
        
        prompt.addAction(datePicker)
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: prompt.view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.6)
        
        prompt.view.addConstraint(height);
        
        let okayJaunt = UIAlertAction(title: "Okay", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            //self.chooseDateAction(type: self.dateType)
            self.okayButtonAction()
        }
        
        prompt.addAction(okayJaunt)
        
        present(prompt, animated: true, completion: nil)
    }
    
    func okayButtonAction(){
        
        let currentDate = Date()
        
        if dateType == "start"{
            if starTimeDate == nil{
                prompt.dismiss(animated: true, completion: nil)
                showAlert(title: "🙃🙃", message: "The START date can't be now.", doAction: true)
                
            } else if currentDate.compare(starTimeDate) == ComparisonResult.orderedSame {
                prompt.dismiss(animated: true, completion: nil)
                showAlert(title: "🙃🙃", message: "The START date can't be now.", doAction: true)
                
            } else if currentDate.compare(starTimeDate) == ComparisonResult.orderedDescending {
                prompt.dismiss(animated: true, completion: nil)
                showAlert(title: "🙃🙃", message: "The START date must be later than the CURRENT date.", doAction: true)
                
            } else {
                prompt.dismiss(animated: true, completion: nil)
            }
            
        } else {
            if starTimeDate == nil{
                prompt.dismiss(animated: true, completion: nil)
                showAlert(title: "🙃🙃", message: "Choose the START date first.", doAction: false)
                endTime.setTitle("End Time", for: .normal)
                
            } else if endTimeDate == nil{
                prompt.dismiss(animated: true, completion: nil)
                showAlert(title: "🙃🙃", message: "The END date must be later than the START date.", doAction: true)
                
            } else if starTimeDate.compare(endTimeDate) == ComparisonResult.orderedSame {
                prompt.dismiss(animated: true, completion: nil)
                showAlert(title: "🙃🙃", message: "The END date must be later than the START date.", doAction: true)
                
            } else if starTimeDate.compare(endTimeDate) == ComparisonResult.orderedDescending{
                prompt.dismiss(animated: true, completion: nil)
                showAlert(title: "🙃🙃", message: "The END date must be later than the START date.", doAction: true)
                
            } else {
                prompt.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func datePickerAction(_ sender: UIDatePicker){
        let timeFormatter = DateFormatter()
        timeFormatter.dateStyle = .short
        timeFormatter.timeStyle = .short
        
        let formattedDate = timeFormatter.string(from: sender.date)
        
        if dateType == "start"{
            startTime.setTitle("\(formattedDate)", for: .normal)
            starTimeDate = sender.date
        } else {
            endTime.setTitle("\(formattedDate)", for: .normal)
            endTimeDate = sender.date
        }
    }
    
    func loadRsvpUsers(){
        let rsvpQuery = PFQuery(className: "RSVP")
        rsvpQuery.whereKey("eventId", equalTo: eventId)
        rsvpQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            for object in objects!{
                let userId = object["userId"] as! String
                
                //append all of the rsvp ids
                self.rsvpIds.append(userId)
            }
        }
    }
    
    func sendNotification(_ user: String, message: String){
        let url = URL(string: "https://hiikey.herokuapp.com/notifications")
        
        Alamofire.request(url!, method: .get, parameters: ["userId":user, "message": message, "eventId": eventId]).responseJSON{ response in
            print(response.request!)
            print(response.response!)
            print(response.result)
        }
    }
    
    func progressBarDisplayer(_ message: String) {
        messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 50, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        messageFrame.addSubview(activityIndicator)
        view.addSubview(messageFrame)
    }
    
    @IBAction func descriptionAction(_ sender: UIButton) {
        view.endEditing(true)
        actionButton.tag = 1
        actionButton.title = "Done"
        self.title = "Description"
        descriptionTextView.becomeFirstResponder()
        self.tableJaunt.isScrollEnabled = false 
        self.view.addSubview(descriptionView)
    }
    
    @objc func exitDescriptionAction(_sender: UIButton){
        self.tableJaunt.isScrollEnabled = true
        
        if descriptionTextView.text == " "{
            descriptionButton.setTitle("description and website links", for: .normal)
            descriptionButton.setTitleColor(UIColor.lightGray, for: .normal)
            
        } else {
            descriptionButton.setTitle(descriptionTextView.text, for: .normal)
            descriptionButton.setTitleColor(UIColor.black, for: .normal)
        }
        
        self.descriptionView.removeFromSuperview()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            if !isUpdating{
                textView.text = ""
            }
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == " " {
            textView.text = "description and website links"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func showAlert(title: String, message: String, doAction: Bool){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if doAction{
            let okayJaunt = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.chooseDateAction(type: self.dateType)
            }
            
            prompt.addAction(okayJaunt)
            
        } else {
            prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        }
        present(prompt, animated: true, completion: nil)
    }
    
    @objc func exitAction(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "welcome") as UIViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
