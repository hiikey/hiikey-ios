//
//  menuHome_ViewController.swift
//  Hiikey
//
//  Created by Mac Owner on 11/17/15.
//  Copyright © 2015 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse
import Kingfisher

class menuHome_ViewController: UIViewController, UIAlertViewDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var userDisplayName: UIButton!
    
    @IBOutlet weak var username: UIButton!

    @IBOutlet weak var hostCount: UIButton!
    
    @IBOutlet weak var guestCount: UIButton!
    ///@IBOutlet weak var userPic: UIImageView!
    
    //pic upload jaunts
    @IBOutlet weak var userPic: UIButton!
    let imagePicker = UIImagePickerController()
    
    //social network handles
    var instagramHandle: String?
    var twitterHandle: String?
    var snapchatHandle: String?
    
    //user host/guest jaunts
    var hostCountInt: Int!
    var guestCountInt: Int!
    
    var hello: String?
    var profileImage: UIImage?
        
    var alert = UIAlertController(title: "Edit Profile", message: "", preferredStyle: .Alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //imagePicker won't won't work without this delegate
        imagePicker.delegate = self
        
        //userPic.layer.cornerRadius = 30
        userPic.layer.cornerRadius = userPic.frame.width / 2
        userPic.clipsToBounds = true
       
        self.loadUserInfo(0, guestCount: 0)
        
        //loadHosts()
    }
    
    //edit DisplayName methods ////////////////////////////////////////////////////////////////
    
    @IBOutlet var newUserDisplayName: UITextField?
    
    //the word typed into the field.. Only function that matters for saving data.
    func wordEntered(alert: UIAlertAction!){
        //var currentDisplayName = userDisplayName.titleLabel?.text
        userDisplayName.setTitle(newUserDisplayName?.text, forState: .Normal)
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackgroundWithId((PFUser.currentUser()?.objectId)!){
            (object: PFObject?, error: NSError?) -> Void in
            if error != nil {
                
            } else if let userDisplayName = object {
                userDisplayName["DisplayName"] = self.newUserDisplayName?.text
                userDisplayName.saveInBackground()
            }
        }
    }

    //places textFiled but important
    func addTextField(textField: UITextField!){
        textField.placeholder = "name"
        textField.text = userDisplayName.titleLabel?.text
        textField.clearButtonMode = UITextFieldViewMode.WhileEditing
        newUserDisplayName = textField
    }
    
    //calls alert
    @IBAction func editName(sender: UIButton) {
        let newWordPrompt = UIAlertController(title: "Edit Name", message: "", preferredStyle: .Alert)
        newWordPrompt.addTextFieldWithConfigurationHandler(addTextField)
        newWordPrompt.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        newWordPrompt.addAction(UIAlertAction(title: "Okay", style: .Default, handler: wordEntered))
        presentViewController(newWordPrompt, animated: true, completion: nil)
    }
    
    //edit instagram methods////////////////////////////////////////////////////////////////////
    
    @IBOutlet var newInstagramName: UITextField?
    
    //the word typed into the field.. Only function that matters for saving data.
    func instagramWordEntered(alert: UIAlertAction!){
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackgroundWithId((PFUser.currentUser()?.objectId)!){
            (object: PFObject?, error: NSError?) -> Void in
            if error != nil {
                
            } else if let userDisplayName = object {
                userDisplayName["InstagramHandle"] = self.newInstagramName?.text
                userDisplayName.saveInBackground()
                self.instagramHandle = self.newInstagramName?.text
            }
        }
    }
    
    //places textFiled but important
    func addInstagramTextField(textField: UITextField!){
        //textField.placeholder = "name"
        textField.text = instagramHandle
        textField.clearButtonMode = UITextFieldViewMode.WhileEditing
        textField.placeholder = "Enter Instagram username"
        newInstagramName = textField
    }

    @IBAction func editInstagram(sender: UIButton) {
        
        let newInstagramHandlePrompt = UIAlertController(title: "Edit Instagram Handle", message: "Grow your Instagram followers by sharing your username.", preferredStyle: .Alert)
        newInstagramHandlePrompt.addTextFieldWithConfigurationHandler(addInstagramTextField)
        newInstagramHandlePrompt.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        newInstagramHandlePrompt.addAction(UIAlertAction(title: "Okay", style: .Default, handler: instagramWordEntered))
        presentViewController(newInstagramHandlePrompt, animated: true, completion: nil)
    }
    
    //Edit Twitter methods ///////////////////////////////////////////////////////////////////////////////
    
    @IBOutlet var newTwitterName: UITextField?
    
    //the word typed into the field.. Only function that matters for saving data.
    func twitterWordEntered(alert: UIAlertAction!){
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackgroundWithId((PFUser.currentUser()?.objectId)!){
            (object: PFObject?, error: NSError?) -> Void in
            if error != nil {
                
            } else if let userDisplayName = object {
                userDisplayName["TwitterHandle"] = self.newTwitterName?.text
                userDisplayName.saveInBackground()
                self.twitterHandle = self.newTwitterName?.text
            }
        }
    }
    
    //places textFiled but important
    func addTwitterTextField(textField: UITextField!){
        textField.text = twitterHandle
        textField.clearButtonMode = UITextFieldViewMode.WhileEditing
        textField.placeholder = "Enter Twitter username"
        newTwitterName = textField
    }

    @IBAction func editTwitter(sender: UIButton) {
        let newTwitterHandlePrompt = UIAlertController(title: "Edit Twitter Handle", message: "Grow your Twitter followers by sharing your username.", preferredStyle: .Alert)
        newTwitterHandlePrompt.addTextFieldWithConfigurationHandler(addTwitterTextField)
        newTwitterHandlePrompt.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        newTwitterHandlePrompt.addAction(UIAlertAction(title: "Okay", style: .Default, handler:twitterWordEntered))
        presentViewController(newTwitterHandlePrompt, animated: true, completion: nil)
    }
    
    //Edit Snapchat methods /////////////////////////////////////////////////////////////////////////////

    @IBOutlet var newSnapchatName: UITextField?
    
    //the word typed into the field.. Only function that matters for saving data.
    func snapchatWordEntered(alert: UIAlertAction!){
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackgroundWithId((PFUser.currentUser()?.objectId)!){
            (object: PFObject?, error: NSError?) -> Void in
            if error != nil {
                
            } else if let userDisplayName = object {
                userDisplayName["SnapchatHandle"] = self.newSnapchatName?.text
                
                userDisplayName.saveInBackground()
                self.snapchatHandle = self.newSnapchatName?.text
            }
        }
    }
    
    //places textFiled but important
    func addSnapChatTextField(textField: UITextField!){
        textField.text = snapchatHandle
        textField.clearButtonMode = UITextFieldViewMode.WhileEditing
        textField.placeholder = "Enter Snapchat username"
        newSnapchatName = textField
    }
    
    @IBAction func editSnapchat(sender: UIButton) {
        
        let newTwitterHandlePrompt = UIAlertController(title: "Edit Snapchat Handle", message: "Grow your Snapchat friends by sharing your username.", preferredStyle: .Alert)
        newTwitterHandlePrompt.addTextFieldWithConfigurationHandler(addSnapChatTextField)
        newTwitterHandlePrompt.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        newTwitterHandlePrompt.addAction(UIAlertAction(title: "Okay", style: .Default, handler:snapchatWordEntered))
        presentViewController(newTwitterHandlePrompt, animated: true, completion: nil)
    }
    
    ////Edit userPIc ////////////////////////////////////////////////////////////////
    
    @IBAction func picUpload(sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    //Image picker functions
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        userPic.setBackgroundImage(chosenImage, forState: .Normal)
        dismissViewControllerAnimated(true, completion: nil)
        
       // let profilePic = UIImagePNGRepresentation(chosenImage)
        let proPic = UIImageJPEGRepresentation(chosenImage, 0.5)
        let imageFile = PFFile(name: "profile_ios.jpeg", data: proPic!)
        imageFile?.saveInBackground()
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackgroundWithId((PFUser.currentUser()?.objectId)!){
            (object: PFObject?, error: NSError?) -> Void in
            if error != nil {
                
            } else if let userDisplayName = object {
                userDisplayName["Profile"] = imageFile
                userDisplayName.saveInBackground()
            }
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func guestAction(sender: UIButton) {
    }
    
    @IBAction func hostAction(sender: UIButton) {
    }
    
    @IBAction func addFriends(sender: UIButton) {
        
        let textToShare = "Add me to your guest list or follow me on @hiikeyapp, it's 🔥 \n"
        
        if let myWebsite = NSURL(string: "https://itunes.apple.com/us/app/hiikey/id1013280340?ls=1&mt=8")
        {
            let objectsToShare = [textToShare, myWebsite]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func signoutAction(sender: UIButton) {
        PFUser.logOut()
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("login") as UIViewController
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    @IBOutlet weak var menuChoseSegment: UISegmentedControl!
    
    @IBAction func menuChoseAction(sender: UISegmentedControl) {
        switch menuChoseSegment.selectedSegmentIndex{
        case 0:
            //home icon
           //do something here where all the data reloads
            break
        
        case 1:
            //Search icon
            let storyboard = UIStoryboard(name: "Explore", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("explore") as UIViewController
            self.presentViewController(controller, animated: true, completion: nil)
            break
            
        case 2:
            //plus icon
            let storyboard = UIStoryboard(name: "Events", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("events") as UIViewController
            self.presentViewController(controller, animated: true, completion: nil)
            break
        
        case 3:
            //fav icon
            let storyboard = UIStoryboard(name: "Mentions", bundle: nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("mentions") as UIViewController
            self.presentViewController(controller, animated: true, completion: nil)
            break
        
        default:
            break
        }
    }
    
    func loadUserInfo(hostCount: Int, guestCount: Int){
        let userQuery = PFQuery(className: "_User")
        userQuery.whereKey("objectId", equalTo: (PFUser.currentUser()?.objectId)!)
        userQuery.findObjectsInBackgroundWithBlock
            {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil
                {
                    if let objects = objects as [PFObject]?
                    {
                        //retrieve the objects from the database
                        for object in objects
                        {
                            self.username.setTitle(object["username"] as? String, forState: .Normal)
                            
                            let displayName = object["DisplayName"] as! String
                            
                            if displayName  == ""{
                                self.userDisplayName.setTitle("Name", forState: .Normal)
                            } else{
                                self.userDisplayName.setTitle(displayName, forState: .Normal)
                            }

                            let userImageUrl: PFFile = object["Profile"] as! PFFile
                            
                            //self.userPic.kf_setBackgroundImageWithURL(NSURL(string: userImageUrl.url!)!, forState: .Normal)
                            
                            self.userPic.kf_setBackgroundImageWithURL(NSURL(string: userImageUrl.url!)!, forState: .Normal, placeholderImage: UIImage(named: "appy.png"))
                            
                           // self.hostCount.setTitle("hosts: \(hostCount)", forState: .Normal)
                           // self.guestCount.setTitle("guests: \(guestCount)", forState: .Normal)
                            
                            self.instagramHandle = object["InstagramHandle"] as? String
                            self.twitterHandle = object["TwitterHandle"] as? String
                            self.snapchatHandle = object["SnapchatHandle"] as? String
                        }
                    }
                    //self.loadViews()
                }
        }
    }
    
    /*func loadHosts(){
        let userQuery = PFQuery(className: "List")
        userQuery.whereKey("guestId", equalTo: (PFUser.currentUser()?.objectId)!)
        userQuery.findObjectsInBackgroundWithBlock
            {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil
                {
                    if let objects = objects as [PFObject]?
                    {
                        self.hostCountInt = objects.count
                       
                    }
                    
                    self.loadGuests()
                }
        }
    }
    
    func loadGuests(){
        let userQuery = PFQuery(className: "List")
        userQuery.whereKey("hostId", equalTo: (PFUser.currentUser()?.objectId)!)
        userQuery.findObjectsInBackgroundWithBlock
            {
                (objects: [PFObject]?, error: NSError?) -> Void in
                if error == nil
                {
                    if let objects = objects as [PFObject]?
                    {
                        self.guestCountInt = objects.count
                    }
                    self.loadUserInfo(self.hostCountInt, guestCount: self.guestCountInt)
                }
        }
    }*/
    
    //an example of how to use object update savinbackgroundwithBlock
    
    /*userDisplayName.saveInBackgroundWithBlock{
    (success: Bool, error: NSError?) -> Void in
    if error != nil{
    
    self.userDisplayName.setTitle(currentDisplayName, forState: .Normal)
    let alertMessage = UIAlertController(title: "Problem with update", message: "Name failed to update, check internet connection.", preferredStyle: .Alert)
    let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
    alertMessage .addAction(action)
    self.presentViewController(alertMessage, animated: true, completion: nil)
    
    } else{
    
    }
    }*/
    //userDisplayName.saveInBackground()
}
