//
//  EventCodeTableViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 1/14/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse

class EventCodeTableViewController: UITableViewController {
    
    @IBOutlet weak var codeTextField: UITextField!
    
     var isEventCode = false
     var codeObjectId: String!
     var codeTitle: String!
     var codeStartDate: Date!
     var codeEndDate: Date!
     var codeLocation: CLLocation!
     var codeAddress: String!
     var codeImageURL: String!
     var codeIsHostSubscribed: Bool!
    
     var codeChatUserIds: String!
     var codeMessages: String!
     var codeCreatedAt: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSchedule"{
            
            let desti = segue.destination as! ScheduleViewController
            
            //event info
            desti.isEventCode = true
            desti.codeObjectId = codeObjectId
            desti.codeTitle = codeTitle
            desti.codeStartDate = codeStartDate
            desti.codeEndDate = codeEndDate
            desti.codeLocation = codeLocation
            desti.codeAddress = codeAddress
            desti.codeImageURL = codeImageURL
            desti.codeIsHostSubscribed = codeIsHostSubscribed
            
            //messages
            desti.codeChatUserIds = codeChatUserIds
            desti.codeMessages = codeMessages
            desti.codeCreatedAt = codeCreatedAt
        }
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        findEvent()
    }
    
    func findEvent(){
        print("checking")
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("code", equalTo: codeTextField.text!)
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                //add variable jaunts
                self.codeObjectId = object?.objectId
                self.codeTitle = object?["title"] as! String
                self.codeStartDate = object?["startTime"] as! Date
                self.codeEndDate = object?["endTime"] as! Date
                
                let pfLocation = object?["location"] as! PFGeoPoint
                let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                
                self.codeLocation = clLocation
                self.codeAddress = object?["address"] as! String
                
                let eventImage: PFFile = object?["eventImage"] as! PFFile
                
                self.codeImageURL = eventImage.url
                self.codeIsHostSubscribed = object?["isSubscribed"] as! Bool
                
                self.codeChatUserIds = object?["userId"] as! String
                self.codeMessages = object?["description"] as! String
                
                let createdAt = DateFormatter.localizedString(from: (object?.createdAt!)!, dateStyle: .short, timeStyle: .short)

                self.codeCreatedAt = createdAt
                self.performSegue(withIdentifier: "showSchedule", sender: self)

            } else {
                self.showAlert()
            }
        }
    }
    
    func showAlert(){
        let newTwitterHandlePrompt = UIAlertController(title: "Couldn't Find the Event", message: "The host may have removed the event or the end date has passed.", preferredStyle: .alert)
        newTwitterHandlePrompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(newTwitterHandlePrompt, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
}
