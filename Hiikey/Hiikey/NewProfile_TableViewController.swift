//
//  NewProfile_TableViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 11/3/16.
//  Copyright © 2016 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse

class NewProfile_TableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var nameField: UITextField!
    
    let imagePicker = UIImagePickerController()
    var uploadedImage: PFFile!
    
    var codeObjectId: String!
    var codeTitle: String!
    var codeStartDate: Date!
    var codeEndDate: Date!
    var codeLocation: CLLocation!
    var codeAddress: String!
    var codeImageURL: String!
    var codeIsHostSubscribed: Bool!
    var codeChatUserIds: String!
    var codeMessages: String!
    var codeCreatedAt: String!
    var codeCode: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.codeCode)
        imagePicker.delegate = self
    }
    
    @IBAction func profileAction(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType =  .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: (PFUser.current()?.objectId)!) {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil && object != nil {
                if self.uploadedImage != nil{
                    object!["Profile"] = self.uploadedImage
                }
                object?["DisplayName"] = self.nameField.text
                object?.saveEventually()
                
                if self.codeCode != "nil"{
                    self.findEvent(self.codeCode)

                } else {
                    self.performSegue(withIdentifier: "showHome", sender: self)
                }
                
            } else {
                print(error!)
            }
        }
    }
    
    //Image picker functions
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        dismiss(animated: true, completion: nil)
        
        profileButton.setBackgroundImage(chosenImage, for: .normal)
        profileButton.setTitle("", for: .normal)
        
        profileButton.layer.cornerRadius = profileButton.frame.width / 2
        profileButton.clipsToBounds = true
        
        let proPic = UIImageJPEGRepresentation(chosenImage, 0.5)
        uploadedImage = PFFile(name: "profile_ios.jpeg", data: proPic!)
        uploadedImage?.saveInBackground()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    func findEvent(_ code: String){
        print("checking")
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("endTime", greaterThan: Date())
        eventQuery.whereKey("code", equalTo: code)
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                
                //add variable jaunts
                self.codeObjectId = object?.objectId
                self.codeTitle = object?["title"] as! String
                self.codeStartDate = object?["startTime"] as! Date
                self.codeEndDate = object?["endTime"] as! Date
                
                let pfLocation = object?["location"] as! PFGeoPoint
                let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                
                self.codeLocation = clLocation
                self.codeAddress = object?["address"] as! String
                
                let eventImage: PFFile = object?["eventImage"] as! PFFile
                
                self.codeImageURL = eventImage.url
                self.codeIsHostSubscribed = object?["isSubscribed"] as! Bool
                
                self.codeChatUserIds = object?["userId"] as! String
                self.codeMessages = object?["description"] as! String
                self.codeCode = object?["code"] as! String
                
                let createdAt = DateFormatter.localizedString(from: (object?.createdAt!)!, dateStyle: .short, timeStyle: .short)
                
                self.codeCreatedAt = createdAt
                
                print("not")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "EventPage") as! EventPageController
                
                //event info
                initialViewController.objectId = self.codeObjectId
                initialViewController.eventTitle = self.codeTitle
                initialViewController.rawStartDate = self.codeStartDate
                initialViewController.rawEndDate = self.codeEndDate
                initialViewController.eventLocation = self.codeLocation
                initialViewController.eventAddress = self.codeAddress
                initialViewController.eventImageURL = self.codeImageURL
                initialViewController.isHostSubscribed = self.codeIsHostSubscribed
                //messages
                
                initialViewController.chatUserIds.append(self.codeChatUserIds)
                initialViewController.messages.append(self.codeMessages)
                initialViewController.createdAt.append(self.codeCreatedAt)
                initialViewController.isLink = true
                
                self.present(initialViewController, animated: true, completion: nil)
                
            } 
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
}
