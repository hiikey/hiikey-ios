//
//  GuestListViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 1/12/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//


import UIKit
import Parse
import Contacts
import Kingfisher
import Social
import Alamofire
import SwiftyJSON
import TwitterKit
import SnapKit

class GuestListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableJaunt: UITableView!

        let countryCodes = ["us":"1", "au": "61", "be":"32", "br":"55", "ca":"1", "cn":"86", "dk":"45", "ec":"593", "eg":"20", "fi":"358","fr":"33", "de":"49", "gh":"233", "gr":"30", "gl":"299", "hk":"852", "hu":"36", "is":"354", "in":"91", "ie":"353", "il":"972", "it":"39", "jp":"81", "ke":"254", "lb":"961", "lu":"352", "mx":"52", "mc":"377", "ma":"212", "nl":"31", "nz":"64", "ni":"505", "ng":"234", "no":"47", "pk":"92", "ps":"970", "py":"595", "pe":"51", "ph":"63", "pl":"48", "pt":"351", "pr":"1", "ro":"40", "ru":"7", "rw":"250", "za":"27", "kr":"82", "es":"34", "se":"46", "ch":"41", "tw":"886", "th":"66", "ae":"971", "ug":"256", "gb":"44", "ua":"380"]
    
    var activityIndicator: UIActivityIndicatorView!
    
    //contact info
    var phoneNumber = [String]()
    var name = [String]()
    var firstName = [String]()
    
    var numberToSend = [String]()
    var nameToSend = [String]()
    
    var isInvited = [Bool]()
    var isHiikeySelected = true
    
    var doneItem: UIBarButtonItem!

    //user info
    var displayName: String!
    
    //event info
    
    var startDateString: String!

    var startDateJaunt: Date!
    var endDateJaunt: Date!
    var descriptionJaunt: String!
    var titleJaunt: String!
    var addressJaunt: String!
    var locationJaunt: PFGeoPoint!
    var eventImageJaunt: PFFile!
    var rawEventImage: UIImage!
    var codeJaunt: String!
    
    var newEventId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Guest List"

        loadContacts()
        if self.eventImageJaunt == nil{
            self.createFlyer()
        }
        
        if PFUser.current() != nil{
            let query = PFQuery(className:"_User")
            query.getObjectInBackground(withId: (PFUser.current()?.objectId)!) {
                (object: PFObject?, error: Error?) -> Void in
                if error == nil && object != nil {
                    self.displayName =  "\(object!["DisplayName"] as! String)(\(PFUser.current()!.username!))"
                }
            }
        }
        
        doneItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(GuestListViewController.doneAction(_:)))
        self.navigationItem.rightBarButtonItem = doneItem
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationNavigationController = segue.destination as! UINavigationController
        
        let desti = destinationNavigationController.topViewController as! NewUserViewController
        
        desti.startDateJaunt = startDateJaunt
        desti.endDateJaunt = endDateJaunt
        desti.descriptionJaunt = descriptionJaunt
        desti.titleJaunt = titleJaunt
        desti.addressJaunt = addressJaunt
        desti.locationJaunt = locationJaunt
        desti.eventImageJaunt = eventImageJaunt
        desti.startDateString = startDateString
        desti.numberToSend = numberToSend
        desti.nameToSend = nameToSend
        if rawEventImage != nil{
            desti.rawEventImage = rawEventImage
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if phoneNumber.count == 0{
            loadContacts()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if phoneNumber.count == 0{
            return 200
        }
        
        return 75
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if phoneNumber.count != 0 {
            return phoneNumber.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: MainTableViewCell!
        
        if phoneNumber.count != 0{
            cell = tableView.dequeueReusableCell(withIdentifier: "guestListReuse", for: indexPath) as! MainTableViewCell
            tableView.separatorStyle = .singleLine
            
            cell.guestName.text = name[indexPath.row]
            cell.guestUsername.text = phoneNumber[indexPath.row]
            
            if !isInvited[indexPath.row]{
                cell.backgroundColor = UIColor.white
                
            } else {
                cell.backgroundColor = uicolorFromHex(0xee1848)
            }
            
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "noContactsReuse", for: indexPath) as! MainTableViewCell
            tableView.separatorStyle = .none
            cell.settingsButton.addTarget(self, action: #selector(GuestListViewController.settingsAction(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //add or remove from guest list
            if !isInvited[indexPath.row]{
                isInvited[indexPath.row] = true
                self.tableJaunt.reloadData()
                
            } else {
                isInvited[indexPath.row] = false
                self.tableJaunt.reloadData()
            }
        
        doneItem.title = "Skip"
        
        for invite in isInvited{
            if invite == true{
                doneItem.title = "Done"
            }
        }
    }
    
    func loadContacts(){
        let toFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        let request = CNContactFetchRequest(keysToFetch: toFetch as [CNKeyDescriptor])
        let store = CNContactStore()
        
        var unSortedNames = [String]()
        var unSortedNumbers = [String]()
        
        do{
            try store.enumerateContacts(with: request) {
                contact, stop in
                
                if contact.phoneNumbers.count != 0{
                    unSortedNames.append(contact.givenName + " " + contact.familyName)
                    self.isInvited.append(false)
                    let phoneString = (contact.phoneNumbers.first?.value)?.value(forKey: "digits") as! String
                    
                    let countryJaunt = (contact.phoneNumbers.first?.value)?.value(forKey: "countryCode") as! String
                    let charset = CharacterSet(charactersIn: "+")
                    
                    //determine if "phoneNumber" has country code prefix
                    if phoneString.rangeOfCharacter(from: charset) != nil{
                        //number already has country code, append to array
                        unSortedNumbers.append(phoneString)
                        
                    } else {
                        //loop through country codes to find one that matches returned country code from current user's contact book
                        for (country, countryCode) in self.countryCodes{
                            //create new contact with country prefix
                            if countryJaunt == country{
                                unSortedNumbers.append("+\(countryCode)\(phoneString)")
                            }
                        }
                    }
                }
            }
            
            //combine the two for sorting the names alphebetically
            let sortInfo = zip(unSortedNames, unSortedNumbers).sorted(by: {$0.0 < $1.0})
            
            //seperate the sorted data into their respective arrays
            self.name = sortInfo.map{$0.0}
            self.phoneNumber = sortInfo.map{$0.1}
            
            self.tableJaunt.reloadData()
            
        } catch let err{
            print(err)
        }
    }
    
    func createEvent(){
        var closedTitle = ""
        let titleBits = titleJaunt!.split{$0 == " "}.map(String.init)
        for object in titleBits{
            closedTitle = closedTitle.lowercased() + object.lowercased()
        }
        codeJaunt = "\(closedTitle)-\(PFUser.current()!.username!)"
        
        //check to see if image was uploaded
        let newEvent = PFObject(className: "Event")
        newEvent["startTime"] = startDateJaunt
        newEvent["endTime"] = endDateJaunt
        newEvent["description"] = descriptionJaunt
        newEvent["title"] = titleJaunt
        newEvent["address"] = addressJaunt
        newEvent["location"] = locationJaunt
        newEvent["userId"] = PFUser.current()?.objectId
        if eventImageJaunt != nil{
            newEvent["eventImage"] = eventImageJaunt
        }

        newEvent["code"] = codeJaunt
        newEvent["isRemoved"] = false
        newEvent["isSubscribed"] = true
        if numberToSend.count != 0{
            newEvent["invites"] = numberToSend
            
        } else {
            newEvent["invites"] = []
        }
        
        newEvent.saveInBackground {
            (success: Bool, error: Error?) -> Void in
            if (success) {
                //notify guest list users
                self.newEventId = newEvent.objectId
            
                self.sendInvites()
                
            } else {
                //show alert that tells the user that the even failed to create
                self.view.isUserInteractionEnabled = true
                self.activityIndicator.removeFromSuperview()
                
                self.showAlert(title: "Problem with Creation", message: "Check internet connection and try again.")
            }
        }
    }

    @objc func doneAction(_ sender: UIBarButtonItem){
        if PFUser.current() != nil{
            progressBarDisplayer()
            self.view.isUserInteractionEnabled = false
            purgeNumbners()

        } else {
            let prompt = UIAlertController(title: "You're Almost Done!", message: "Hiikey will create your event and send out the invites after you sign up.", preferredStyle: .actionSheet)
            
            let signUpAction = UIAlertAction(title: "Sign Up", style: .default) {
                UIAlertAction in
                
                self.purgeNumbners()

                self.performSegue(withIdentifier: "showNewUser", sender: self)
            }
            
            prompt.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            prompt.addAction(signUpAction)
            present(prompt, animated: true, completion: nil)
        }
    }
    
    func purgeNumbners(){
        var name = [String]()
        
        var i = 0
        
        for invite in isInvited{
            if invite{
                numberToSend.append(self.phoneNumber[i])
                name.append(self.name[i])
                
                //get first name from full name
                let fullname = self.name[i].split{$0 == " "}.map(String.init)
                nameToSend.append(fullname[0])
            }
            i += 1
        }
        
        if PFUser.current() != nil{
            createEvent()
        }
    }
    
    func sendInvites(){
        
        let url = URL(string: "https://hiikey.herokuapp.com/invites")
        
        let paramerters = [
            "phoneNumbers": numberToSend,
            "names": nameToSend,
            "hostname": displayName,
            "eventName": titleJaunt,
            "id": newEventId,
            "eventDate": startDateString
            
            ] as [String : Any]
        
        Alamofire.request(url!, method: .get, parameters: paramerters ).responseJSON{ response in
            print(response.request!)
            print(response.response!)
            print(response.result)
            
            //look at response and determine if was a success
            
            let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "shareView") as! ShareViewController
            //event info
            initialViewController.eventTitle = self.titleJaunt!
            initialViewController.eventId = self.newEventId!
            
            if self.rawEventImage != nil{
                initialViewController.eventImage = self.rawEventImage
                initialViewController.isImage = true
            }
            
            self.present(initialViewController, animated: true, completion: nil)
        }
    }
    
    func progressBarDisplayer() {
        let messageFrame = UIView(frame: CGRect(x: view.frame.midX, y: view.frame.midY - 25 , width: 50, height: 50))
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        messageFrame.addSubview(activityIndicator)
        view.addSubview(messageFrame)
    }
    
    @objc func settingsAction(_ sender: UIButton) {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: nil)
        }
    }

    func sendNotification(_ user: String, message: String){
        let url = URL(string: "https://hiikey.herokuapp.com/notifications")
        
        Alamofire.request(url!, method: .get, parameters: ["userId":user, "message": message]).responseJSON{ response in
        }
    }
    
    func showAlert(title: String, message: String){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(prompt, animated: true, completion: nil)
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    func createFlyer(){
        let flyerView = UIView(frame: CGRect(x: 0, y: 0, width: 816, height: 1056))

        flyerView.backgroundColor = self.uicolorFromHex(0x180d22)
        
        let flyerTitle = UILabel(frame: CGRect(x: 0, y: 350, width: flyerView.frame.width - 10, height: 80))
        flyerTitle.text = self.titleJaunt
        flyerTitle.textColor = self.uicolorFromHex(0xee1848)
        flyerTitle.font = UIFont(name: "ArialRoundedMTBold", size: 70)
        flyerTitle.textAlignment = .center
        flyerTitle.numberOfLines = 3
        flyerView.addSubview(flyerTitle)

        let flyerDate = UILabel(frame: CGRect(x: 0, y: 500, width: flyerView.frame.width - 10, height: 60))

        flyerDate.text = self.startDateString
        flyerDate.textColor = self.uicolorFromHex(0xee1848)
        flyerDate.textAlignment = .center
        flyerDate.font = UIFont(name: "ArialRoundedMTBold", size: 60)
        flyerDate.numberOfLines = 3
        flyerView.addSubview(flyerDate)
        
        let flyerImage = UIImage(view: flyerView)
        
        let proPic = UIImageJPEGRepresentation(flyerImage, 0.5)
        self.eventImageJaunt = PFFile(name: "eventImage_ios.jpeg", data: proPic!)
        self.eventImageJaunt.saveInBackground()
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
    }
}
