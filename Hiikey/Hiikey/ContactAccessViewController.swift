//
//  ContactAccessViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 2/7/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Contacts
import Parse

class ContactAccessViewController: UIViewController {

    @IBOutlet weak var linkContactsButton: UIButton!
    
    var defaults = UserDefaults.standard
    
    //event info
    var startDateString: String!
    var startDateJaunt: Date!
    var endDateJaunt: Date!
    var descriptionJaunt: String!
    var titleJaunt: String!
    var addressJaunt: String!
    var locationJaunt: PFGeoPoint!
    var eventImageJaunt: PFFile!
    var codeJaunt: String!
    var rawEventImage: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let desti = segue.destination as! GuestListViewController
        desti.startDateJaunt = startDateJaunt
        desti.endDateJaunt = endDateJaunt
        desti.descriptionJaunt = descriptionJaunt
        desti.titleJaunt = titleJaunt
        desti.addressJaunt = addressJaunt
        desti.locationJaunt = locationJaunt
        if eventImageJaunt != nil{
            desti.eventImageJaunt = eventImageJaunt
        }

        desti.startDateString = startDateString
        
        if rawEventImage != nil{
            desti.rawEventImage = rawEventImage
        }
    }
    
    @IBAction func linkContactsAction(_ sender: UIButton) {
        self.defaults.set(true, forKey: "showContactAccess")
        self.defaults.synchronize()
        
        switch CNContactStore.authorizationStatus(for: .contacts){
        case .authorized:
            self.performSegue(withIdentifier: "showGuestList", sender: self)
            break
            
        case .notDetermined:
            self.performSegue(withIdentifier: "showGuestList", sender: self)
            break

        default:
            self.performSegue(withIdentifier: "showGuestList", sender: self)
        }
    }
}
