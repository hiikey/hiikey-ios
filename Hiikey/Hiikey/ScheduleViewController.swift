//
//  ScheduleViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 1/3/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse
import Kingfisher
import SnapKit

import FBSDKCoreKit
import FBSDKShareKit

class ScheduleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FBSDKAppInviteDialogDelegate {
    /**
     Sent to the delegate when the app invite completes without error.
     - Parameter appInviteDialog: The FBSDKAppInviteDialog that completed.
     - Parameter results: The results from the dialog.  This may be nil or empty.
     */
    public func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
    }

    @IBOutlet weak var tableJaunt: UITableView!
    
    var refresh: UIRefreshControl!
    
    //Current user info
    var userRSVPs = [String]()
    var userNumber = ""
    
    //scheduled events
    var eventImage = [String]()
    var eventTitle = [String]()
    var eventStartDate = [String]()
    var eventEndDate = [String]()
    var rawEventStartDate = [Date]()
    var rawEventEndDate = [Date]()
    var eventObjectId = [String]()
    var eventUserId = [String]()
    var userLocation: PFGeoPoint!
    var eventDescription = [String]()
    var eventLocation = [CLLocation]()
    var eventAddress = [String]()
    var eventCreatedAt = [String]()
    var isHostSubscribed = [Bool]()
    var eventCode = [String]()
    var eventInvites = [Array<String>]()
    
    var blockedEvents = [String]()
    
    //event code variables
    var isEventCode = false
    
    var codeObjectId: String!
    var codeTitle: String!
    var codeStartDate: Date!
    var codeEndDate: Date!
    var codeLocation: CLLocation!
    var codeAddress: String!
    var codeImageURL: String!
    var codeIsHostSubscribed: Bool!
    var codeChatUserIds: String!
    var codeMessages: String!
    var codeCreatedAt: String!
    var codeCode: String!
    var codeInvites = [String]()
    
    var activityIndicator: UIActivityIndicatorView!
    
    //blocked
    var isBlocked = false
    
    lazy var eventCodeTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont(name: "HelveticaNeue", size: 16)
        textField.placeholder = "Event Code"
        textField.clearButtonMode = .whileEditing
        return textField
    }()
    
    lazy var titleButton: UIButton = {
        let button =  UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
        //button.backgroundColor = UIColor.redColor()
        button.setTitle("", for: .normal)
        button.setTitleColor(UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0), for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        button.addTarget(self, action: #selector(ScheduleViewController.editProfileAction(_:)), for: UIControlEvents.touchUpInside)
        
        return button
    }()
    
    lazy var titleImage: UIImageView = {
        let button =  UIImageView(frame: CGRect(x: -30, y: 5, width: 30, height: 30))
        button.layer.cornerRadius = 15
        button.clipsToBounds = true
        return button
    }()
    
    //index
    var selectedIndex: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadRequests()
        
        //setup tableview
        self.tableJaunt.register(MainTableViewCell.self, forCellReuseIdentifier: "ProfileCell")
        self.tableJaunt.register(MainTableViewCell.self, forCellReuseIdentifier: "EventCell")
        self.tableJaunt.estimatedRowHeight = 50
        self.tableJaunt.rowHeight = UITableViewAutomaticDimension
        
        loadProfile()
        
        refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "")
        refresh.addTarget(self, action: #selector(ScheduleViewController.loadRSVPs(_:)), for: UIControlEvents.valueChanged)
        tableJaunt.addSubview(refresh)
        
        self.refresh.beginRefreshing()        
        
        self.loadRSVPs(refresh)

    }
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print("Error in invite \(error)")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEventPage"{
            
            let desti = segue.destination as! EventPageController
            
            if isEventCode{
                //event info
                desti.objectId = codeObjectId
                desti.eventTitle = codeTitle
                desti.rawStartDate = codeStartDate
                desti.rawEndDate = codeEndDate
                desti.eventLocation = codeLocation
                desti.eventAddress = codeAddress
                if codeImageURL != nil{
                    desti.eventImageURL = codeImageURL
                }

                desti.isHostSubscribed = codeIsHostSubscribed
                desti.eventCode = codeCode
                desti.invites = codeInvites
                
                //messages
                desti.chatUserIds.append(codeChatUserIds)
                desti.messages.append(codeMessages)
                desti.createdAt.append(codeCreatedAt)
                desti.chatIds.append("description")
            
            } else{
                //event info
                desti.objectId = eventObjectId[selectedIndex]
                desti.eventTitle = eventTitle[selectedIndex]
                desti.rawStartDate = rawEventStartDate[selectedIndex]
                desti.rawEndDate = rawEventEndDate[selectedIndex]
                desti.eventLocation = eventLocation[selectedIndex]
                desti.eventAddress = eventAddress[selectedIndex]
                if eventImage[selectedIndex] != " "{
                    desti.eventImageURL = eventImage[selectedIndex]
                }

                desti.isHostSubscribed = isHostSubscribed[selectedIndex]
                desti.eventCode = eventCode[selectedIndex]
                desti.invites = eventInvites[selectedIndex]
                //messages
                desti.chatUserIds.append(eventUserId[selectedIndex])
                desti.messages.append(eventDescription[selectedIndex])
                desti.createdAt.append(eventCreatedAt[selectedIndex])
                desti.chatIds.append("description")
            }
            
            desti.userNumber = userNumber
        }
    }
    
    //mark - tableview
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Plans"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventObjectId.count == 0{
            return 1
            
        } else {
            return eventObjectId.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: MainTableViewCell!
        
        if eventObjectId.count == 0{
            cell = tableView.dequeueReusableCell(withIdentifier: "nothingScheduledReuse", for: indexPath) as! MainTableViewCell
            self.tableJaunt.separatorStyle = .none
            
            if !refresh.isRefreshing{
                cell.noPlansLabel.isHidden = false
                cell.noPlansExplain.isHidden = false
            }
            
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! MainTableViewCell
            
            self.tableJaunt.separatorStyle = .singleLine
            
            cell.selectionStyle = .none
            if eventImage[indexPath.row] != " "{
                cell.eventImage.kf.setImage(with: URL(string: eventImage[indexPath.row])! , placeholder: UIImage(named: "appy"), options: nil, progressBlock: nil, completionHandler: nil)
                
            } else {
                cell.eventDescription.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(cell.eventDate.snp.bottom).offset(20)
                    make.left.equalTo(cell).offset(10)
                    make.right.equalTo(cell).offset(-10)
                    make.bottom.equalTo(cell).offset(-20)
                }
            }
            
            cell.eventTitle.text = eventTitle[indexPath.row]
            cell.eventDate.text = eventStartDate[indexPath.row]
            cell.eventDescription.text = eventDescription[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //if person has access, show event page
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "showEventPage", sender: self)
    }
    
    //mark - mich
    func loadProfile(){
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: PFUser.current()!.objectId!) {
            (object: PFObject?, error: Error?) -> Void in
            
            let proPic: PFFile = object!["Profile"] as! PFFile
            
            self.titleButton.setTitle("\(object!["username"] as! String)", for: .normal)
            self.titleImage.kf.setImage(with: URL(string: proPic.url!))
            self.titleButton.addSubview(self.titleImage)
            
            self.navigationItem.titleView = self.titleButton

            if object?["phoneNumber"] != nil{
                self.userNumber = object?["phoneNumber"] as! String
            }
        }
    }
    
    @objc func loadRSVPs(_ refreshControl: UIRefreshControl){
        let eventQuery = PFQuery(className: "RSVP")
        eventQuery.whereKey("userId", equalTo: PFUser.current()!.objectId!)
        eventQuery.whereKey("isBlocked", equalTo: false)
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil{
                if let objects = objects as [PFObject]?{
                    //retrieve the objects from the database
                    for object in objects{
                        self.userRSVPs.append(object["eventId"] as! String)
                    }
                }
                self.loadEvents()
            }
        }
    }
    
    func loadEvents(){
        let rsvp = PFQuery(className:"Event")
        rsvp.whereKey("objectId", containedIn: userRSVPs)
        
        let myEvents = PFQuery(className:"Event")
        myEvents.whereKey("userId", equalTo: PFUser.current()!.objectId!)
        
        let query = PFQuery.orQuery(withSubqueries: [rsvp, myEvents])
        query.addAscendingOrder("startTime")
        query.whereKey("objectId", notContainedIn: eventObjectId)
        query.whereKey("endTime", greaterThan: Date())
        query.whereKey("isRemoved", equalTo: false)
        query.findObjectsInBackground {
            (results: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = results as [PFObject]?{
                    //retrieve the objects from the database
                    for object in objects{
                        let rawEndTime = object["endTime"] as! Date
                        let rawStartTime = object["startTime"] as! Date
                        
                        let startDate = DateFormatter.localizedString(from: rawStartTime, dateStyle: .medium, timeStyle: .short)
                        self.eventStartDate.append(startDate)
                        self.rawEventStartDate.append(rawStartTime)
                        
                        let endDate = DateFormatter.localizedString(from: rawEndTime, dateStyle: .medium, timeStyle: .short)
                        self.eventEndDate.append(endDate)
                        self.rawEventEndDate.append(rawEndTime)
                        
                        if object["eventImage"] != nil{
                            let image: PFFile = object["eventImage"] as! PFFile
                            self.eventImage.append(image.url!)
                        } else {
                            self.eventImage.append(" ")
                        }
                        
                        self.eventTitle.append(object["title"] as! String)
                        self.eventUserId.append(object["userId"] as! String)
                        self.eventDescription.append(object["description"] as! String)
                        self.eventObjectId.append(object.objectId!)
                        self.eventAddress.append(object["address"] as! String)
                        self.eventCode.append(object["code"] as! String)
                        let pfLocation = object["location"] as! PFGeoPoint
                        let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                        self.eventLocation.append(clLocation)
                        
                        let createdAt = DateFormatter.localizedString(from: object.createdAt!, dateStyle: .short, timeStyle: .short)
                        
                        self.eventCreatedAt.append(createdAt)
                        
                        self.isHostSubscribed.append(object["isSubscribed"] as! Bool)
                        
                        self.eventInvites.append(object["invites"] as! Array<String>)
                    }
                }
                
                self.refresh.endRefreshing()
                self.tableJaunt.reloadData()
            }
        }
    }
    
    func findEvent(){
        print("checking")
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("code", equalTo: eventCodeTextField.text!.lowercased())
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                self.isEventCode = true
                
                //add variable jaunts
                self.codeObjectId = object?.objectId
                self.codeTitle = object?["title"] as! String
                self.codeStartDate = object?["startTime"] as! Date
                self.codeEndDate = object?["endTime"] as! Date
                
                let pfLocation = object?["location"] as! PFGeoPoint
                let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                
                self.codeLocation = clLocation
                self.codeAddress = object?["address"] as! String
                if object?["eventImage"] != nil{
                    let eventImage: PFFile = object?["eventImage"] as! PFFile
                    self.codeImageURL = eventImage.url

                }

                self.codeIsHostSubscribed = object?["isSubscribed"] as! Bool
                
                self.codeChatUserIds = object?["userId"] as! String
                self.codeMessages = object?["description"] as! String
                self.codeCode = object?["code"] as! String
                self.codeInvites = object?["invites"] as! Array<String>
                
                let createdAt = DateFormatter.localizedString(from: (object?.createdAt!)!, dateStyle: .short, timeStyle: .short)
                
                self.codeCreatedAt = createdAt
                
                var isBlocked = false
                for id in self.blockedEvents{
                    if self.codeObjectId == id{
                        self.showAlert(title: "You've been Blocked from this event.", message: "")
                        isBlocked = true
                    }
                }
                
                if !isBlocked{
                    self.performSegue(withIdentifier: "showEventPage", sender: self)
                }
                
            } else {
                self.isEventCode = false
                self.showAlert(title: "Couldn't find the event", message: "The end date may have passed or the host removed the event.")
            }
        }
    }
    
    func loadRequests(){
        let eventQuery = PFQuery(className: "RSVP")
        eventQuery.whereKey("eventHostId", equalTo: PFUser.current()!.objectId!)
        eventQuery.addDescendingOrder("createdAt")
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.whereKey("isConfirmed", equalTo: false)
        eventQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil{
                if objects?.count != 0{
                    self.tabBarController?.tabBar.items?[3].badgeValue = "\(objects!.count)"
                    self.tabBarController?.tabBar.items?[3].badgeColor = self.uicolorFromHex(0xee1848)
                }
            }
        }
    }
    
    @IBAction func eventCodeAction(_ sender: UIBarButtonItem) {
        let prompt = UIAlertController(title: "Find Event", message: "", preferredStyle: .alert)
        
        prompt.view.addSubview(self.eventCodeTextField)
        
        eventCodeTextField.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(prompt.view).offset(25)
            make.left.equalTo(prompt.view).offset(10)
            make.right.equalTo(prompt.view).offset(-10)
            make.bottom.equalTo(prompt.view).offset(-25)
        }
        
        let search = UIAlertAction(title: "Search", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.findEvent()
        }
        
        prompt.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        prompt.addAction(search)
        present(prompt, animated: true, completion: nil)
    }
    
    @IBAction func shareProfileAction(_ sender: UIBarButtonItem) {
        let currentUser = PFUser.current()!.username!
        let myWebsite = NSURL(string: "https://www.hiikey.com/u/\(currentUser)")
            print(myWebsite!)
            var objectsToShare = [AnyObject]()
            objectsToShare.append(myWebsite!)
            let activityVC = UIActivityViewController(activityItems: objectsToShare , applicationActivities: nil)
            self.present(activityVC, animated: true, completion: nil)
    }
    
    @objc func editProfileAction(_ sender: UIButton){
        self.performSegue(withIdentifier: "showProfile", sender: self)
    }
    
    func progressBarDisplayer() {
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityIndicator.frame = CGRect(x: view.frame.midX - 25, y: view.frame.midY - 25, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        view.addSubview(activityIndicator)
    }
    
    func showAlert(title: String, message: String){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(prompt, animated: true, completion: nil)
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}


