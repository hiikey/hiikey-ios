//
//  LoginViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 2/9/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController {
    
    lazy var eventCodeTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont(name: "HelveticaNeue", size: 16)
        textField.placeholder = "email"
        textField.clearButtonMode = .whileEditing
        return textField
    }()

    var defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let exitItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(LoginViewController.exitAction(_:)))
        self.navigationItem.leftBarButtonItem = exitItem
    }
        
    var valUsername = false
    var valPassword = false
    var valEmail = false
    
    var signupHidden = true
    
    let uiProfilePic = UIImage(named: "appy.jpeg")
    
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
        
    var imageFile: PFFile!

    @IBAction func agreementAction(_ sender: UIButton){
        UIApplication.shared.open(URL(string: "https://www.hiikey.com/terms" )!)
    }
    
    @IBAction func logAction(_ sender: UIButton) {
        returningUser( password: password.text!, username: (username.text!).lowercased())
    }
    
    @IBAction func forgotPasswordAction(_ sender: UIBarButtonItem) {
        let prompt = UIAlertController(title: "Reset Password", message: "", preferredStyle: .alert)
        
        prompt.view.addSubview(self.eventCodeTextField)
        
        eventCodeTextField.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(prompt.view).offset(25)
            make.left.equalTo(prompt.view).offset(10)
            make.right.equalTo(prompt.view).offset(-10)
            make.bottom.equalTo(prompt.view).offset(-25)
        }
        
        let search = UIAlertAction(title: "Reset", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            
            let blockQuery = PFQuery(className: "_User")
            blockQuery.whereKey("email", equalTo: "\(self.eventCodeTextField.text!.lowercased())")
            blockQuery.findObjectsInBackground{
                (objects: [PFObject]?, error: Error?) -> Void in
                if objects?.count != 0{
                    PFUser.requestPasswordResetForEmail(inBackground: "\(self.eventCodeTextField.text!.lowercased())")
                    self.promptJaunt(message: "Click on the link from noreply@hiikey.com", title: "Check your inbox" )
                    
                } else {
                    self.promptJaunt(message: "Couldn't find an email associated with \(self.eventCodeTextField.text!.lowercased())", title: "Oops" )
                }
            }
        }
        
        prompt.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        prompt.addAction(search)
        present(prompt, animated: true, completion: nil)
    }
    
    func promptJaunt(message: String, title: String){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(prompt, animated: true, completion: nil)
    }
    
    func validateUsername() ->Bool{
        if username.text!.isEmpty{
            
            username.attributedPlaceholder = NSAttributedString(string:"Field required",
                                                                attributes:[NSAttributedStringKey.foregroundColor: UIColor.red])
            valUsername = false
            
        } else if (username.text?.rangeOfCharacter(from: NSCharacterSet.whitespaces)) != nil{
            
            let fullNameArr = username.text?.split{$0 == " "}.map(String.init)
            username.text = ""
            for object in fullNameArr!{
                username.text = username.text! + object
            }
            valUsername = true
        } else{
            print("true")
            valUsername = true
        }
        return valUsername
    }
    
    func validatePassword() -> Bool{
        if password.text!.isEmpty{
            password.attributedPlaceholder = NSAttributedString(string:"Field required",
                                                                attributes:[NSAttributedStringKey.foregroundColor: UIColor.red])
            valPassword = false
            
        } else{
            print("true")
            valPassword = true
        }
        
        return valPassword
    }
    
    func returningUser( password: String, username: String){
        
        let userJaunt = username.trimmingCharacters(
            in: NSCharacterSet.whitespacesAndNewlines
        )
        
        PFUser.logInWithUsername(inBackground: userJaunt, password:password) {
            (user: PFUser?, error: Error?) -> Void in
            if user != nil {
                
                //associate current user with device
                let installation = PFInstallation.current()
                installation?["user"] = PFUser.current()
                installation?["userId"] = PFUser.current()?.objectId
                installation?.saveEventually()
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "main") as UIViewController
                self.present(controller, animated: true, completion: nil)
                
            } else {
                self.showAlert("Login Attempt Unsuccessful", message: "Check username and password combo.")
                print("fail")
            }
        }
    }
    
    @objc func exitAction(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "welcome") as UIViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    func showAlert(_ title: String, message: String){
        //make action for forgot password jaunt
        let forgotPasswordAction = UIAlertAction(title: "ResetPassword", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.performSegue(withIdentifier: "forgotPasswordSegue", sender: nil)
        }
        
        let newTwitterHandlePrompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        newTwitterHandlePrompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        newTwitterHandlePrompt.addAction(forgotPasswordAction)
        present(newTwitterHandlePrompt, animated: true, completion: nil)
    }

}
