//
//  NewUserViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 2/7/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//
//  Was about to connect all of the event info to be sent to editProfile

import UIKit
import Parse
import DigitsKit

class NewUserViewController: UIViewController {
    
    //UI components
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    
    //validate jaunts
    var valUsername = false
    var valPassword = false
    var valEmail = false
    
    //event info
    var startDateString: String!
    var startDateJaunt: Date!
    var endDateJaunt: Date!
    var descriptionJaunt: String!
    var titleJaunt: String!
    var addressJaunt: String!
    var locationJaunt: PFGeoPoint!
    var eventImageJaunt: PFFile!
    var codeJaunt: String!
    var rawEventImage: UIImage!
    
    //invites
    var numberToSend = [String]()
    var nameToSend = [String]()

    //type of signup
    var isJoiningEvent = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        let exitItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(NewUserViewController.exitAction(_:)))
        self.navigationItem.leftBarButtonItem = exitItem
        
        self.title = "Sign up for Hiikey"
        let doneItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(NewUserViewController.next(_:)))
        self.navigationItem.rightBarButtonItem = doneItem
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let desti = segue.destination as! EditProfile_TableViewController
        
        //user info
        desti.userNameString = usernameField.text!
        desti.emailString = emailField.text!
        desti.passwordString = passwordField.text!
        if isJoiningEvent{
            desti.isJoiningEvent = true
            desti.codeJaunt = codeJaunt
            
        } else{
            //event info
            desti.startDateJaunt = startDateJaunt
            desti.endDateJaunt = endDateJaunt
            desti.descriptionJaunt = descriptionJaunt
            desti.titleJaunt = titleJaunt
            desti.addressJaunt = addressJaunt
            desti.locationJaunt = locationJaunt
            if eventImageJaunt != nil{
                desti.eventImageJaunt = eventImageJaunt
            }

            desti.startDateString = startDateString
            
            //invites
            desti.numberToSend = numberToSend
            desti.nameToSend = nameToSend
            
            if rawEventImage != nil{
                desti.rawEventImage = rawEventImage
            }
        }
    }
    
    @objc func next(_ sender: UIBarButtonItem){
        if validateEmail() && validatePassword() && validateUsername(){
            let usernameQuery = PFQuery(className: "_User")
            usernameQuery.whereKey("username", equalTo: usernameField.text! )
            usernameQuery.findObjectsInBackground{
                (objects: [PFObject]?, error: Error?) -> Void in
                if objects?.count == 0{
                    let emailQuery = PFQuery(className: "_User")
                     emailQuery.whereKey("email", equalTo: self.emailField.text!  )
                    emailQuery.findObjectsInBackground{
                        (objects: [PFObject]?, error: Error?) -> Void in
                        if objects?.count == 0{
                            self.performSegue(withIdentifier: "showNewProfile", sender: self)
                            
                        } else {
                            self.showAlert("Email already in use", message: "")
                        }
                    }
                    
                } else {
                    self.showAlert("Username already in use", message: "")
                }
            }
        }
    }
    
    //MARK: Validate jaunts

    func validateUsername() ->Bool{
        //Validate username
        
        if usernameField.text!.isEmpty{
            usernameField.attributedPlaceholder = NSAttributedString(string:"Field required",
                                                                attributes:[NSAttributedStringKey.foregroundColor: UIColor.red])
            valUsername = false
            
        } else if (usernameField.text?.rangeOfCharacter(from: NSCharacterSet.whitespaces)) != nil{
            let fullNameArr = usernameField.text?.split{$0 == " "}.map(String.init)
            usernameField.text = ""
            for object in fullNameArr!{
                usernameField.text = usernameField.text! + object.lowercased()
            }
        
            valUsername = true
            
        } else{
            valUsername = true
        }
        return valUsername
    }
    
    func validatePassword() -> Bool{
        if passwordField.text!.isEmpty{
            passwordField.attributedPlaceholder = NSAttributedString(string:"Field required",
                                                                attributes:[NSAttributedStringKey.foregroundColor: UIColor.red])
            valPassword = false
            
        } else{
            valPassword = true
        }
        
        return valPassword
    }
    
    func validateEmail() -> Bool{
        let emailString : NSString = emailField.text! as NSString
        if emailField.text!.isEmpty{
            emailField.attributedPlaceholder = NSAttributedString(string:"Field required",
                                                             attributes:[NSAttributedStringKey.foregroundColor: UIColor.red])
            valEmail = false
            view.endEditing(true)
            
        } else if !emailString.contains("@"){
            emailField.text = ""
            emailField.attributedPlaceholder = NSAttributedString(string:"Valid email required",
                                                             attributes:[NSAttributedStringKey.foregroundColor: UIColor.red])
            valEmail = false
            view.endEditing(true)
            
        } else if !emailString.contains("."){
            emailField.text = ""
            emailField.attributedPlaceholder = NSAttributedString(string:"Valid email required",
                                                             attributes:[NSAttributedStringKey.foregroundColor: UIColor.red])
            valEmail = false
            view.endEditing(true)
            
        } else{
            valEmail = true
        }
        return valEmail
    }
    
    @objc func exitAction(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "welcome") as UIViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func termsAction(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: "https://www.hiikey.com/terms" )!)
     }
    
    func showAlert(_ title: String, message: String){
        let newTwitterHandlePrompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        newTwitterHandlePrompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(newTwitterHandlePrompt, animated: true, completion: nil)
    }
}
