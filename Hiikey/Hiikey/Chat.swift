//
//  Chat.swift
//  Hiikey
//
//  Created by Dominic Smith on 1/1/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse

public class Chat: PFObject, PFSubclassing {
    public class func parseClassName() -> String {
        return "Chat"
    }
    
    @NSManaged public var eventId: String!
    @NSManaged public var userId: String!
    @NSManaged public var icon: PFFile!
    @NSManaged public var message: String!
    @NSManaged public var supportURLs: [NSString: String]!
    @NSManaged public var librariesURLs: [NSString: String]!
    
}
