//
//  AppDelegate.swift
//  Hiikey
//
//  Created by Dominic Smith on 5/12/15.
//  Copyright (c) 2015 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse
import Bolts
import Fabric
import DigitsKit
import TwitterKit
import FacebookCore
import FacebookLogin

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var codeObjectId: String!
    var codeTitle: String!
    var codeStartDate: Date!
    var codeEndDate: Date!
    var codeLocation: CLLocation!
    var codeAddress: String!
    var codeImageURL: String!
    var codeIsHostSubscribed: Bool!
    var codeChatUserIds: String!
    var codeMessages: String!
    var codeCreatedAt: String!
    var codeCode: String!
    var invites = [String]()
    var window: UIWindow?
 var defaults = UserDefaults.standard
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Digits.self, Twitter.self])
        
        //change color of time/status jaunts to white
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for:UIControlState())
        
        UITabBar.appearance().tintColor = uicolorFromHex(0xee1848)
        
        let navigationBarAppearace = UINavigationBar.appearance()
        
        navigationBarAppearace.barTintColor = uicolorFromHex(0xffffff)
        navigationBarAppearace.tintColor = uicolorFromHex(0xee1848)
        
        // Override point for customization after application launch.
        Parse.enableLocalDatastore()
        
        // Initialize Parse.
                
        //MARK - this is the real one below
        let configuration = ParseClientConfiguration {
            $0.applicationId = "O9M9IE9aXxHHaKmA21FpQ1SR26EdP2rf4obYxzBF"
            $0.clientKey = "LdoCUneRAIK9sJLFFhBFwz2YkW02wChG5yi2wkk2"
            $0.server = "https://hiikey.herokuapp.com/parse"
            //$0.server = "http://192.168.1.66:3000/parse"
        }
        Parse.initialize(with: configuration)
        
        // [Optional] Track statistics around application opens.
        PFAnalytics.trackAppOpened(launchOptions: launchOptions)
        
        //Notification Jaunts.. see: https://www.parse.com/docs/ios/guide#push-notifications-setting-up-push
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        if PFUser.current() != nil{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "main")
            
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
        } else {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "welcome")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        
        return SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        return SDKApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            let url = userActivity.webpageURL!
            //handle url
            
            //let components = URLComponents(url: url, resolvingAgainstBaseURL: true)
            print(url.path)
            
            if url.path == "/events"{
                let urlBits = url.absoluteString.characters.split{$0 == "="}.map(String.init)
                
                findEvent(urlBits[1])
                return true
                
            } else {
                return false
            }
        }
        
        return false
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        let installation = PFInstallation.current()
        //replace badge # when someone clicks on notification
        installation?.badge = 0
        installation?.saveInBackground()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Store the deviceToken in the current Installation and save it to Parse
        let installation = PFInstallation.current()
        installation?.setDeviceTokenFrom(deviceToken)
        //replace badge # when someone clicks on notification
        installation?.badge = 0
        installation?.saveInBackground()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if ( application.applicationState == UIApplicationState.background ){
            PFPush.handle(userInfo)
        }
    }
    
    func findEvent(_ code: String){
        print("checking")
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("endTime", greaterThan: Date())
        eventQuery.whereKey("objectId", equalTo: code)
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                //self.isEventCode = true
                
                //add variable jaunts
                self.codeObjectId = object?.objectId
                self.codeTitle = object?["title"] as! String
                self.codeStartDate = object?["startTime"] as! Date
                self.codeEndDate = object?["endTime"] as! Date
                
                let pfLocation = object?["location"] as! PFGeoPoint
                let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                
                self.codeLocation = clLocation
                self.codeAddress = object?["address"] as! String
                
                let eventImage: PFFile = object?["eventImage"] as! PFFile
                
                self.codeImageURL = eventImage.url
                self.codeIsHostSubscribed = object?["isSubscribed"] as! Bool
                
                self.codeChatUserIds = object?["userId"] as! String
                self.codeMessages = object?["description"] as! String
                self.codeCode = object?["code"] as! String
                self.invites = object?["invites"] as! Array<String>
                let createdAt = DateFormatter.localizedString(from: (object?.createdAt!)!, dateStyle: .short, timeStyle: .short)
                
                self.codeCreatedAt = createdAt
                
               self.intiateView()
                
            } 
        }
    }
    
    func intiateView(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "EventPage") as! EventPageController
        
        
        //event info
        initialViewController.objectId = self.codeObjectId
        initialViewController.eventTitle = self.codeTitle
        initialViewController.rawStartDate = self.codeStartDate
        initialViewController.rawEndDate = self.codeEndDate
        initialViewController.eventLocation = self.codeLocation
        initialViewController.eventAddress = self.codeAddress
        initialViewController.eventImageURL = self.codeImageURL
        initialViewController.isHostSubscribed = self.codeIsHostSubscribed
        initialViewController.eventCode = self.codeCode
        initialViewController.invites = self.invites
        //messages
        
        initialViewController.chatUserIds.append(self.codeChatUserIds)
        initialViewController.messages.append(self.codeMessages)
        initialViewController.createdAt.append(self.codeCreatedAt)
        initialViewController.chatIds.append("description")
        initialViewController.isLink = true
        
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }
    
    //define custom color jaunts..  see https://coderwall.com/p/dyqrfa/customize-navigation-bar-appearance-with-swift for reference
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}

