//
//  WelcomeViewController.swift
//  Hiikey
//
//  Created by Dominic Smith on 2/6/17.
//  Copyright © 2017 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import Parse 
import SnapKit

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var planButton: UIButton!
    
    @IBOutlet weak var joinButton: UIButton!
    
    var codeObjectId: String!
    var codeTitle: String!
    var codeStartDate: Date!
    var codeEndDate: Date!
    var codeLocation: CLLocation!
    var codeAddress: String!
    var codeImageURL: String!
    var codeIsHostSubscribed: Bool!
    var codeChatUserIds: String!
    var codeMessages: String!
    var codeCreatedAt: String!
    var codeCode: String!
    
    lazy var eventCodeTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont(name: "HelveticaNeue", size: 16)
        textField.placeholder = "Event Code"
        textField.clearButtonMode = .whileEditing
        return textField
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        planButton.layer.cornerRadius = 5
        planButton.clipsToBounds = true
        
        joinButton.layer.cornerRadius = 5
        joinButton.clipsToBounds = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationNavigationController = segue.destination as! UINavigationController
        
        if segue.identifier == "showNewEvent"{
            let targetController = destinationNavigationController.topViewController as! NewEvent_TableViewController
            targetController.isNewUser = true
        }
    }

    func findEvent(_ code: String){
        let eventQuery = PFQuery(className: "Event")
        eventQuery.whereKey("endTime", greaterThan: Date())
        eventQuery.whereKey("code", equalTo: code)
        eventQuery.whereKey("isRemoved", equalTo: false)
        eventQuery.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                
                //add variable jaunts
                self.codeObjectId = object?.objectId
                self.codeTitle = object?["title"] as! String
                self.codeStartDate = object?["startTime"] as! Date
                self.codeEndDate = object?["endTime"] as! Date
                
                let pfLocation = object?["location"] as! PFGeoPoint
                let clLocation = CLLocation(latitude: pfLocation.latitude, longitude: pfLocation.longitude)
                
                self.codeLocation = clLocation
                self.codeAddress = object?["address"] as! String
                
                let eventImage: PFFile = object?["eventImage"] as! PFFile
                
                self.codeImageURL = eventImage.url
                self.codeIsHostSubscribed = object?["isSubscribed"] as! Bool
                
                self.codeChatUserIds = object?["userId"] as! String
                self.codeMessages = object?["description"] as! String
                self.codeCode = object?["code"] as! String
                
                let createdAt = DateFormatter.localizedString(from: (object?.createdAt!)!, dateStyle: .short, timeStyle: .short)
                
                self.codeCreatedAt = createdAt
                
                self.goToEventPage()
                
            } else {
                let prompt = UIAlertController(title: "Couldn't Find The Event", message: "The event date may have passed or the host removed it from Hiikey.", preferredStyle: .alert)
                prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                self.present(prompt, animated: true, completion: nil)
            }
        }
    }
    
    func goToEventPage(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "EventPage") as! EventPageController
        //event info
        initialViewController.objectId = self.codeObjectId
        initialViewController.eventTitle = self.codeTitle
        initialViewController.rawStartDate = self.codeStartDate
        initialViewController.rawEndDate = self.codeEndDate
        initialViewController.eventLocation = self.codeLocation
        initialViewController.eventAddress = self.codeAddress
        initialViewController.eventImageURL = self.codeImageURL
        initialViewController.isHostSubscribed = self.codeIsHostSubscribed
        initialViewController.eventCode = self.codeCode
        
        //messages
        initialViewController.chatUserIds.append(self.codeChatUserIds)
        initialViewController.messages.append(self.codeMessages)
        initialViewController.createdAt.append(self.codeCreatedAt)
        initialViewController.isLink = true
        
        self.present(initialViewController, animated: true, completion: nil)
    }
}
