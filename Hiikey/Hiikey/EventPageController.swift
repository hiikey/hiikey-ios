//
//  EventPageController.swift
//  Hiikey
//
//  Created by Mac Owner on 12/29/16.
//  Copyright © 2016 SocialGroupe Incorporated. All rights reserved.
//
//HANDLE Chat SUBSCRIBING AND UNSCRIBING TO AVOID CLOGGING SHIT UP WITH SCALE

//FOR SOME REASON CAN'T LOAD CHATS... SOMETHING WITCH JAUNT RELAODING
import UIKit
import SlackTextViewController
import Parse
//import ParseLiveQuery
import SnapKit
import CoreLocation
import Alamofire
import EventKit
import Kingfisher 
import SKPhotoBrowser

class EventPageController: SLKTextViewController {
    
    @IBOutlet weak var flyerBackground: UIImageView!
    //people Reuse
    var isRSVPerSubscribed = [Bool]()
    var userImageURL = [String]()
    var userObjectId = [String]()
    var userName = [String]()
    var userDisplayName = [String]()
    var userID = [String]()
    var isCurrentUserSubscribed = true
    
    //messages
    var chatUserRSVPs = [String]()
    var chatUserImageURL = [String]()
    var chatUserName = [String]()
    var chatUserDisplayName = [String]()
    
    var messages = [String]()
    var chatUserIds = [String]()
    var chatIds = [String]()
    var createdAt = [String]()
    var isEventHost = [Bool]()
    
    var searchResult: [String]?
    
    //event info
    var hasUserRSVP = false
    var areMessagesShowing = true
    
    var objectId: String!
    var eventTitle: String!
    var rawStartDate: Date!
    var rawEndDate: Date!
    var eventAddress: String!
    var eventLocation: CLLocation!
    var eventImageURL: String!
    var eventDescription: String!
    var isHostSubscribed: Bool!
    var eventCode: String!
    var invites = [String]()
    var blockedUsers = [String]()
    
    var eventCity = "Loading"

    var rsvpIds = [String]()
    
    var rsvpButton: UIButton!
    var activityIndicator: UIActivityIndicatorView!
    
    var subscribeItem: UIBarButtonItem!
    
    var eventImage: UIImage!
    
    var replyUsername = [String]()
    
    //Activity Variables
    var activityUsername: String!
    var activityMessageId: String!
    
    //invites
    var userNumber = ""
    var isInvited = false 
    
    //if user clicked on Hiikey link... navigation bar isn't showing... so add another cell
    var isLink = false
    var navBar: UINavigationBar = UINavigationBar()

    lazy var userImage: UIImageView = {
        let image = UIImageView(frame: CGRect(x: 10, y: 0, width: 50, height: 50))
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        image.image = UIImage(named: "testImage")
        image.contentMode = .scaleAspectFill
        return image
    }()

    /*let liveQueryClient = ParseLiveQuery.Client()
    private var subscription: Subscription<Chat>?
    var messagesQuery: PFQuery<Chat>{
        return (Chat.query()!
            .whereKey("eventId", equalTo: objectId) as! PFQuery<Chat> )
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        //setup tableview
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: "EventInfoCell")
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: "MessageTableViewCell")
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: "RSVPCell")
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: "exitCell")
        
        self.tableView?.estimatedRowHeight = 50
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        //makes newest messages show at top
        self.isInverted = false
        
        self.textView.isHidden = true
        self.textView.placeholder = "Message \(eventTitle!)"
        
        if activityUsername != nil{
            self.textView.text = "@\(activityUsername!)"
        }
        
        self.rightButton.setTitleColor(uicolorFromHex(0xee1848), for: .normal)
        
        //auto complete for mentions
        self.autoCompletionView.register(MainTableViewCell.self, forCellReuseIdentifier: "RSVPCell")
        self.autoCompletionView.estimatedRowHeight = 50
        self.autoCompletionView.rowHeight = UITableViewAutomaticDimension
        self.registerPrefixes(forAutoCompletion: ["@"])
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicator.frame = CGRect(x: self.view.frame.width / 2 - 50, y: 0, width: 50, height: 50)
        
        rsvpButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        rsvpButton.setTitle("RSVP", for: .normal)
        rsvpButton.backgroundColor = uicolorFromHex(0xee1848)
        rsvpButton.setTitleColor(UIColor.white, for: .normal)
        rsvpButton.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 20)
        rsvpButton.addTarget(self, action: #selector(EventPageController.didTapRSVP), for: .touchUpInside)
        
        for invite in invites{
            if invite == userNumber{
                isInvited = true
            }
        }
        
        if PFUser.current() != nil{
            loadBlockedUsers()

        } else {
            didUserRSVP()
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editEvent"{
            let desti = segue.destination as! NewEvent_TableViewController
            
            let startDate = DateFormatter.localizedString(from: rawStartDate!, dateStyle: .short, timeStyle: .short)
            let endDate = DateFormatter.localizedString(from: rawEndDate!, dateStyle: .short, timeStyle: .short)
            
            desti.eventId = objectId
            desti.titleJaunt = eventTitle
            desti.addressJaunt = eventAddress
            desti.descriptionJaunt = self.messages[0]
            desti.startJaunt = startDate
            desti.endJaunt = endDate
            desti.imageJaunt = eventImageURL
            desti.starTimeDate = rawStartDate
            desti.endTimeDate = rawEndDate
            desti.isUpdating = true
            desti.addressBool = true
            
        } else {
            let destinationNavigationController = segue.destination as! UINavigationController
            let desti = destinationNavigationController.topViewController as! NewUserViewController
            desti.codeJaunt = eventCode
            desti.isJoiningEvent = true 
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    //mark - chat
    
    //did press send button
    override func didChangeAutoCompletionPrefix(_ prefix: String, andWord word: String) {
        
        var array:Array<String> = []
        let wordPredicate = NSPredicate(format: "self BEGINSWITH[c] %@", word);
        
        self.searchResult = nil
        
        if prefix == "@" {
            if word.count > 0 {
                array = self.userName.filter { wordPredicate.evaluate(with: $0) };
            }
                
            else {
                array = self.userName
            }
        }
        
        var show = false
        
        if array.count > 0 {
            let sortedArray = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            self.searchResult = sortedArray
            show = sortedArray.count > 0
        }
        self.showAutoCompletionView(show)
    }
    
    override func didPressRightButton(_ sender: Any?) {
        let message = self.textView.text
        
        let newMessage = PFObject(className: "Chat")
        newMessage["eventId"] = objectId
        //change to current user
        newMessage["userId"] = PFUser.current()?.objectId
        newMessage["message"] = self.textView.text
        newMessage.saveEventually {
            (success: Bool, error: Error?) -> Void in
            if (success) {
                self.textView.text = ""
                self.view.endEditing(true)
                
                self.configureMentions(message: message!, objectId: newMessage.objectId!)
            }
        }
    }
    
    //mark - tableview
    override func heightForAutoCompletionView() -> CGFloat {
        guard self.searchResult != nil else {
            return 0
        }
        
        return 400 
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        if isLink{
            return 3

        } else {
            if tableView == self.tableView{
                return 2
                
            } else {
                return 1
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView != self.autoCompletionView{
            if isLink{
                if section == 0{
                    return ""
                    
                } else if section == 1{
                    return ""
                    
                } else if areMessagesShowing {
                    return "Messages"
                    
                } else {
                    return "RSVP's"
                }
                
            } else {
                if section == 0{
                    return ""
                    
                } else if areMessagesShowing {
                    return "Messages"
                    
                } else {
                    return "RSVP's"
                }
            }
            
        } else {
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = .white
        let segmentedControl = UISegmentedControl(frame: CGRect(x: 10, y: 0, width: tableView.frame.width - 20, height: 30))
        segmentedControl.insertSegment(withTitle: "\(messages.count) Messages", at: 0, animated: false)
        segmentedControl.insertSegment(withTitle: "\(rsvpIds.count) Guests", at: 1, animated: false)
        segmentedControl.tintColor = uicolorFromHex(0xee1848)
        if areMessagesShowing{
            segmentedControl.selectedSegmentIndex = 0

        } else {
            segmentedControl.selectedSegmentIndex = 1

        }
        segmentedControl.addTarget(self, action: #selector(EventPageController.viewRSVPUsers(_:)), for: .valueChanged)
        v.addSubview(segmentedControl)
        return v
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLink{
            if section == 0{
                return 1
                
            } else if section == 1{
                return 1
                
            } else {
                if hasUserRSVP{
                    if areMessagesShowing{
                        return messages.count
                        
                    } else {
                        return rsvpIds.count
                    }
                    
                } else {
                    return 1
                }
            }
            
        } else {
            if section == 0{
                return 1
                
            } else {
                if tableView == self.tableView {
                    if hasUserRSVP{
                        if areMessagesShowing{
                            return messages.count
                            
                        } else {
                            return rsvpIds.count
                        }
                        
                    } else {
                        return 1
                    }
                    
                }else {
                    if let searchResult = self.searchResult {
                        if self.chatUserIds[0] == PFUser.current()?.objectId{
                            return searchResult.count + 1

                        } else {
                            return searchResult.count
                        }
                    }
                }
            }
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            return self.messageCellForRowAtIndexPath(indexPath)
        }
            
        else {
            return self.autoCompletionCellForRowAtIndexPath(indexPath)
        }
    }
    
    func autoCompletionCellForRowAtIndexPath(_ indexPath: IndexPath) -> MainTableViewCell {
        let cell = self.autoCompletionView.dequeueReusableCell(withIdentifier: "RSVPCell") as! MainTableViewCell
        cell.selectionStyle = .default
        
        let userQuery = PFQuery(className: "_User")
        userQuery.whereKey("username", equalTo: searchResult![indexPath.row])
        userQuery.cachePolicy = .cacheElseNetwork
        userQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            if objects != nil{
                for object in objects! {
                    let proPic: PFFile = object["Profile"] as! PFFile
                    
                    cell.userImage.kf.setImage(with: URL(string: proPic.url!)!, placeholder: UIImage(named: "appy"))
                    cell.nameLabel.text = object["DisplayName"] as? String
                    cell.usernameLabel.text = "@\(object["username"] as! String)"
                }
            }
        }
        
        return cell
    }
    
    func messageCellForRowAtIndexPath(_ indexPath: IndexPath) -> MainTableViewCell {
        var cell: MainTableViewCell!
        
        if isLink{
            
            if indexPath.section == 0{
                cell = self.tableView?.dequeueReusableCell(withIdentifier: "exitCell", for: indexPath) as! MainTableViewCell
                
                cell.exitButton.addTarget(self, action: #selector(EventPageController.exitAction(_:)), for: .touchUpInside)
                
            } else if indexPath.section == 1{
                cell = self.tableView?.dequeueReusableCell(withIdentifier: "EventInfoCell", for: indexPath) as! MainTableViewCell
                
                //title
                cell.titleLabel.text = eventTitle
                let startDate = DateFormatter.localizedString(from: rawStartDate!, dateStyle: .short, timeStyle: .short)
                let endDate = DateFormatter.localizedString(from: rawEndDate!, dateStyle: .short, timeStyle: .short)
                
                //image
                cell.flyerButton.kf.setBackgroundImage(with: URL(string: eventImageURL)!, for: .normal)
                eventImage = cell.flyerButton.backgroundImage(for: .normal)
                cell.flyerButton.addTarget(self, action: #selector(EventPageController.viewImage(_:)), for: .touchUpInside)
                
                //date
                cell.dateLabel.text = "\(startDate) - \(endDate)"
                
                //address
                if hasUserRSVP{
                    cell.addressButton.setTitle(eventAddress, for: .normal)
                    cell.addressButton.isUserInteractionEnabled = true
                    cell.addressButton.setTitleColor(UIColor.black, for: .normal)
                } else {
                    cell.addressButton.setTitle(eventCity, for: .normal)
                    cell.addressButton.setTitleColor(UIColor.lightGray, for: .normal)
                }
                
                cell.addressButton.addTarget(self, action: #selector(EventPageController.getDirections(_:)), for: .touchUpInside)
                
            } else {
                cell = self.tableView?.dequeueReusableCell(withIdentifier:
                    "MessageTableViewCell", for: indexPath) as! MainTableViewCell
                cell.selectionStyle = .none
                
                cell.bodyLabel.handleURLTap { url in
                    if url.absoluteString.contains("https") || url.absoluteString.contains("http"){
                        UIApplication.shared.open(url)
                        
                    } else {
                        UIApplication.shared.open(URL(string: "https://\(url)")!)
                    }
                }
                
                //means user data hasn't been loaded yer
                if hasUserRSVP{
                    if areMessagesShowing{
                        cell.countLabel.isHidden = true
                        
                        if self.chatUserIds[indexPath.row] == self.chatUserIds[0]{
                            cell.hostMarker.isHidden = false
                            
                        } else {
                            cell.hostMarker.isHidden = true
                        }
                        
                        cell.bodyLabel.text = self.messages[indexPath.row]
                        
                        cell.createdAt.text = self.createdAt[indexPath.row]
                        
                        cell.replyButton.isHidden = false
                        cell.replyButton.addTarget(self, action: #selector(EventPageController.replyAction(_:)), for: .touchUpInside)
                        cell.replyButton.tag = indexPath.row
                        
                        let query = PFQuery(className:"_User")
                        query.cachePolicy = .cacheElseNetwork
                        query.getObjectInBackground(withId: self.chatUserIds[indexPath.row]) {
                            (object: PFObject?, error: Error?) -> Void in
                            cell.nameLabel.text = object?["DisplayName"] as? String
                            self.replyUsername.insert("@\(object!["username"] as! String)", at: indexPath.row)
                            let proPic: PFFile = object!["Profile"] as! PFFile
                            cell.userImage.kf.setImage(with: URL(string: proPic.url!)!, placeholder: UIImage(named: "appy"))
                        }
                        
                    } else {
                        cell = self.tableView?.dequeueReusableCell(withIdentifier: "RSVPCell", for: indexPath) as! MainTableViewCell
                        cell.selectionStyle = .none
                        cell.userImage.kf.setImage(with: URL(string: self.userImageURL[indexPath.row])!, placeholder: UIImage(named: "appy"))
                        cell.nameLabel.text = self.userDisplayName[indexPath.row]
                        cell.usernameLabel.text = "@\(self.userName[indexPath.row])"
                    }
                    
                } else {
                    cell.countLabel.isHidden = false
                    cell.bodyLabel.text = self.messages[0]
                    cell.createdAt.text = self.createdAt[0]
                    let query = PFQuery(className:"_User")
                    query.cachePolicy = .cacheElseNetwork
                    query.getObjectInBackground(withId: self.chatUserIds[0]) {
                        (object: PFObject?, error: Error?) -> Void in
                        
                        cell.nameLabel.text = object?["username"] as? String
                        
                        let proPic: PFFile = object!["Profile"] as! PFFile
                        cell.userImage.kf.setImage(with: URL(string: proPic.url!)!, placeholder: UIImage(named: "appy"))
                    }
                    cell.replyButton.isHidden = true
                    cell.addSubview(cell.countLabel)
                    cell.countLabel.snp.makeConstraints { (make) -> Void in
                        make.top.equalTo(cell.bodyLabel.snp.bottom).offset(20)
                        make.left.equalTo(cell).offset(15)
                        make.right.equalTo(cell).offset(-5)
                        make.bottom.equalTo(cell).offset(-20)
                    }
                    cell.countLabel.text = "\(self.messages.count - 1) more messages"
                }
            }
            
        } else {
            //if not link
            if indexPath.section == 0{
                cell = self.tableView?.dequeueReusableCell(withIdentifier: "EventInfoCell", for: indexPath) as! MainTableViewCell
                
                //title
                if eventImageURL != nil{
                    cell.flyerButton.kf.setBackgroundImage(with: URL(string: eventImageURL)!, for: .normal)
                    eventImage = cell.flyerButton.backgroundImage(for: .normal)
                    cell.flyerButton.addTarget(self, action: #selector(EventPageController.viewImage(_:)), for: .touchUpInside)
                }

                cell.titleLabel.text = eventTitle
                let startDate = DateFormatter.localizedString(from: rawStartDate!, dateStyle: .short, timeStyle: .short)
                let endDate = DateFormatter.localizedString(from: rawEndDate!, dateStyle: .short, timeStyle: .short)
                
                //date
                cell.dateLabel.text = "\(startDate) - \(endDate)"
                
                //address
                if hasUserRSVP{
                    cell.addressButton.setTitle(eventAddress, for: .normal)
                    cell.addressButton.isUserInteractionEnabled = true
                    cell.addressButton.setTitleColor(UIColor.black, for: .normal)
                } else {
                    cell.addressButton.setTitle(eventCity, for: .normal)
                    cell.addressButton.setTitleColor(UIColor.lightGray, for: .normal)
                }
                
                cell.addressButton.addTarget(self, action: #selector(EventPageController.getDirections(_:)), for: .touchUpInside)
                
                cell.selectionStyle = .none
                
            } else if indexPath.section == 1 {
                cell = self.tableView?.dequeueReusableCell(withIdentifier:
                    "MessageTableViewCell", for: indexPath) as! MainTableViewCell
                
                //change the background of cell if user is coming from activity view
                if activityMessageId != nil{
                    if activityMessageId == self.chatIds[indexPath.row]{
                        cell.backgroundColor = UIColor.lightGray
                        
                    } else {
                        cell.backgroundColor = UIColor.white
                    }
                }
                
                cell.selectionStyle = .none
                
                cell.bodyLabel.handleURLTap { url in
                    if url.absoluteString.contains("https") || url.absoluteString.contains("http"){
                        UIApplication.shared.open(url)
                        
                    } else {
                        UIApplication.shared.open(URL(string: "https://\(url)")!)
                    }
                }
                
                //messages
                //means user data hasn't been loaded yer
                if hasUserRSVP{
                    if areMessagesShowing{
                        cell.countLabel.isHidden = true
                        
                        if self.chatUserIds[indexPath.row] == self.chatUserIds[0]{
                            cell.hostMarker.isHidden = false
                            
                        } else {
                             cell.hostMarker.isHidden = true 
                        }
                        
                        cell.bodyLabel.text = self.messages[indexPath.row]
                        
                        cell.createdAt.text = self.createdAt[indexPath.row]
                        
                        cell.replyButton.isHidden = false
                        cell.replyButton.addTarget(self, action: #selector(EventPageController.replyAction(_:)), for: .touchUpInside)
                        cell.replyButton.tag = indexPath.row
                        
                        let query = PFQuery(className:"_User")
                        query.cachePolicy = .cacheElseNetwork
                        query.getObjectInBackground(withId: self.chatUserIds[indexPath.row]) {
                            (object: PFObject?, error: Error?) -> Void in
                            cell.nameLabel.text = object?["DisplayName"] as? String
                            self.replyUsername.insert("@\(object!["username"] as! String)", at: indexPath.row)
                            let proPic: PFFile = object!["Profile"] as! PFFile
                            cell.userImage.kf.setImage(with: URL(string: proPic.url!)!, placeholder: UIImage(named: "appy"))
                        }
                        
                    } else {
                        cell = self.tableView?.dequeueReusableCell(withIdentifier: "RSVPCell", for: indexPath) as! MainTableViewCell
                        cell.selectionStyle = .none
                        cell.userImage.kf.setImage(with: URL(string: self.userImageURL[indexPath.row])!, placeholder: UIImage(named: "appy"))
                        cell.nameLabel.text = self.userDisplayName[indexPath.row]
                        cell.usernameLabel.text = "@\(self.userName[indexPath.row])"
                    }
                    
                } else {
                    cell.countLabel.isHidden = false 
                    cell.bodyLabel.text = self.messages[0]
                    cell.createdAt.text = self.createdAt[0]
                    let query = PFQuery(className:"_User")
                    query.cachePolicy = .cacheElseNetwork
                    query.getObjectInBackground(withId: self.chatUserIds[0]) {
                        (object: PFObject?, error: Error?) -> Void in
                        
                        cell.nameLabel.text = object?["username"] as? String
                        
                        let proPic: PFFile = object!["Profile"] as! PFFile
                        cell.userImage.kf.setImage(with: URL(string: proPic.url!)!, placeholder: UIImage(named: "appy"))
                    }
                    cell.replyButton.isHidden = true
                    cell.addSubview(cell.countLabel)
                    cell.countLabel.snp.makeConstraints { (make) -> Void in
                        make.top.equalTo(cell.bodyLabel.snp.bottom).offset(20)
                        make.left.equalTo(cell).offset(15)
                        make.right.equalTo(cell).offset(-5)
                        make.bottom.equalTo(cell).offset(-20)
                    }
                    cell.countLabel.text = "\(self.messages.count - 1) more messages"
                }
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.autoCompletionView {
            
            guard let searchResult = self.searchResult else {
                return
            }
            
            var item = searchResult[(indexPath as NSIndexPath).row]
            
            item += " "
            
            self.acceptAutoCompletion(with: item, keepPrefix: true)
            
        } else {
            let objectId = PFUser.current()?.objectId
            
            if indexPath.section == 1{
                if areMessagesShowing{
                    if indexPath.row != 0{
                        let blockPrompt = UIAlertController(title: "Report Message?", message: "", preferredStyle: .actionSheet)
                        
                        let confirmBlock = UIAlertAction(title: "YES", style: .default) { (action) in
                            
                            let block = PFObject(className: "Report")
                            
                            block["postId"] = self.objectId
                            block["userId"] = PFUser.current()?.objectId
                            block["isRemoved"] = false
                            block["type"] = "message: \(self.messages[indexPath.row])"
                            block.saveInBackground(block: { (success: Bool, error: Error?) -> Void in
                                
                            })
                        }
                        blockPrompt.addAction(confirmBlock)
                        blockPrompt.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
                        present(blockPrompt, animated: true, completion: nil)
                    }
                    
                } else if self.chatUserIds[0] == objectId{
                    
                    let blockPrompt = UIAlertController(title: "Block \(self.userDisplayName[indexPath.row]) from \(self.eventTitle!)?", message: "\(self.userDisplayName[indexPath.row]) cannot be unblocked later. \n", preferredStyle: .actionSheet)
                    
                    let confirmBlock = UIAlertAction(title: "Confirm Block", style: .default) { (action) in
                        
                        let ticketQuery = PFQuery(className: "RSVP")
                        ticketQuery.whereKey("eventId", equalTo: self.objectId)
                        ticketQuery.whereKey("userId", equalTo: self.userID[indexPath.row])
                        ticketQuery.findObjectsInBackground{
                            (objects: [PFObject]?, error: Error?) -> Void in
                            if objects != nil{
                                for object in objects! {
                                    
                                    object["isBlocked"] = true
                                    object.saveInBackground(block: { (success: Bool, error: Error?) -> Void in
                                        self.rsvpIds.remove(at: indexPath.row)
                                        self.userDisplayName.remove(at: indexPath.row)
                                        self.userName.remove(at: indexPath.row)
                                        self.userImageURL.remove(at: indexPath.row)
                                        self.tableView?.reloadData()
                                    })
                                }
                            }
                        }
                        
                    }
                    blockPrompt.addAction(confirmBlock)
                    blockPrompt.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    present(blockPrompt, animated: true, completion: nil)
                }
            }
        }
    }
    
    //mark - mich.
    func loadMessages(){
        let ticketQuery = PFQuery(className: "Chat")
        ticketQuery.whereKey("eventId", equalTo: objectId)
        ticketQuery.addDescendingOrder("createdAt")
        ticketQuery.findObjectsInBackground{
                (objects: [PFObject]?, error: Error?) -> Void in
                if objects != nil{
                    for object in objects! {
                        let userId = object["userId"] as! String
                        
                        self.messages.append(object["message"] as! String)
                        self.chatUserIds.append(userId)
                        
                        //so person can access corrent username when replying
                        self.replyUsername.append("")
                        
                        self.chatIds.append(object.objectId!)
                        
                        let rawCreatedAt = object.createdAt
                        let createdAt = DateFormatter.localizedString(from: rawCreatedAt!, dateStyle: .short, timeStyle: .short)
                        
                        self.createdAt.append(createdAt)
                        
                    }
                    self.tableView?.reloadData()
                    
                    //find message to scroll to if user is coming from activity view
                    if self.activityMessageId != nil{
                        var i = 0
                        for id in self.chatIds{
                            if id == self.activityMessageId{
                                let indexPath = NSIndexPath(row: i, section: 1)
                                self.tableView?.scrollToRow(at: indexPath as IndexPath, at: .top , animated: true)
                            }
                            i += 1
                        }
                    }
                    self.startChatSubscription()
                }
        }
    }
    
    func startChatSubscription(){

        /*self.subscription = self.liveQueryClient
            .subscribe(self.messagesQuery)
            .handle(Event.created) { _, message in
                //loaduser info here
                
                //insert new message below the host's description message
                let createdAt = DateFormatter.localizedString(from: Date(), dateStyle: .short, timeStyle: .short)
                
                //self.chatUserIds.append(message["userId"] as! String)
                self.chatUserIds.insert(message["userId"] as! String, at: 1)
               // self.messages.append(message["message"] as! String)
                self.messages.insert(message["message"] as! String, at: 1)
               // self.createdAt.append(createdAt)
                self.createdAt.insert(createdAt, at: 1)
                self.chatIds.insert(message.objectId!, at: 1)
                
                self.tableView?.reloadData()
        }*/
    }
    
    @objc func viewRSVPUsers(_ sender: UISegmentedControl){
        //segue to show rsvp users
        if sender.selectedSegmentIndex == 0{
            
            //go to top if messages are already showing
            if areMessagesShowing{
                self.tableView?.setContentOffset(CGPoint.zero, animated: true)
            }
            
            areMessagesShowing = true
            
            self.tableView?.reloadData()
            
        } else {
            if !areMessagesShowing{
                self.tableView?.setContentOffset(CGPoint.zero, animated: true)
            }
            
            areMessagesShowing = false
            self.tableView?.reloadData()
        }
    }
    
    func loadRSVPers(){
        let userQuery = PFQuery(className: "_User")
        userQuery.whereKey("objectId", containedIn: self.rsvpIds)
        userQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            if objects != nil{
                for object in objects! {
                    
                    let proPic: PFFile = object["Profile"] as! PFFile
                    self.userImageURL.append(proPic.url!)
                    
                    self.userName.append(object["username"] as! String)
                    self.userDisplayName.append(object["DisplayName"] as! String)
                    
                    self.userID.append(object.objectId!)
                }
            }
        }
    }
    
    func didUserRSVP(){
        var didRSVP = false
        var isCurrentUser = false
        var isSubscribed = true
        var isConfirmed = false
        var isBlocked = false
        
        //check to see if the current user is the host ... they don't need to rsvp obvi
        //check to see if current user is host\
        
        let currentUser = PFUser.current()
        
        if currentUser != nil{
            if self.chatUserIds[0] == currentUser!.objectId! {
                didRSVP = true
                isCurrentUser = true
                isConfirmed = true 
                isSubscribed = self.isHostSubscribed
                
                //if not, see if current user is subscribed / rsvp'd
            }
        }

        let rsvpQuery = PFQuery(className: "RSVP")
        rsvpQuery.whereKey("eventId", equalTo: objectId)
        rsvpQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            for object in objects!{
                let userId = object["userId"] as! String
                
                if PFUser.current() != nil{
                    if userId == PFUser.current()!.objectId! {
                        isSubscribed = object["isSubscribed"] as! Bool
                        didRSVP = true
                        isConfirmed = object["isConfirmed"] as! Bool
                        if object["isBlocked"] != nil{
                            isBlocked = object["isBlocked"] as! Bool
                        }
                    }
                }

                //used for subscribing and un-subscribing to messages... see "subscribeaction()"
                self.isCurrentUserSubscribed = isSubscribed
                
                //check to see if guest is blocked.. don't want to show up in rsvp list if blocked
                let isGuestBlocked = object["isBlocked"] as! Bool
                
                //append all of the rsvp ids if the user has access to the event
                if object["isConfirmed"] as! Bool {
                    if !isGuestBlocked{
                        self.rsvpIds.append(userId)
                        self.isRSVPerSubscribed.append(isSubscribed)
                    }
                }
            }
            
            if didRSVP{
                if isBlocked{
                    self.hasUserRSVP = false
                    self.textView.isHidden = true
                    self.rsvpButton.setTitle("You're blocked from this event", for: .normal)
                    self.rsvpButton.setTitleColor(UIColor.white, for: .normal)
                    self.rsvpButton.backgroundColor = UIColor.black
                    self.rsvpButton.isUserInteractionEnabled = false
                    self.textView.superview?.addSubview(self.rsvpButton)
                    self.locationCity()
                    self.createNavigation(false, isSubscribed: false, isConfirmed: false)
                    
                } else if isConfirmed{
                    self.hasUserRSVP = true
                    self.textView.isHidden = false
                    self.loadMessages()
                    self.createNavigation(isCurrentUser, isSubscribed: isSubscribed, isConfirmed: true)
                    
                } else {
                    self.textView.isHidden = true 
                    self.rsvpButton.setTitle("RSVP Pending", for: .normal)
                    self.rsvpButton.setTitleColor(UIColor.white, for: .normal)
                    self.rsvpButton.backgroundColor = UIColor.darkGray
                    self.rsvpButton.isUserInteractionEnabled = false
                    self.textView.superview?.addSubview(self.rsvpButton)
                    self.locationCity()
                    self.createNavigation(false, isSubscribed: false, isConfirmed: false)
                }
                
            } else{
                self.textView.superview?.addSubview(self.rsvpButton)
                self.locationCity()
                self.createNavigation(false, isSubscribed: false, isConfirmed: false)
            }
            self.loadRSVPers()
        }
    }
    
    @objc func didTapRSVP(){
        if PFUser.current() != nil{
            activityIndicator.startAnimating()
            self.textView.superview?.addSubview(activityIndicator)
            
            var notificationMessage = "RSVP'd to"
            
            let newRSVP = PFObject(className: "RSVP")
            newRSVP["eventId"] = objectId
            newRSVP["userId"] = PFUser.current()?.objectId
            newRSVP["eventHostId"] = chatUserIds[0]
            newRSVP["eventTitle"] = eventTitle
            newRSVP["isSubscribed"] = true
            newRSVP["isConfirmed"] = isInvited
            newRSVP["isRemoved"] = false
            newRSVP["isBlocked"] = false 
            newRSVP.saveInBackground {
                (success: Bool, error: Error?) -> Void in
                if (success) {
                    self.activityIndicator.removeFromSuperview()
                    if self.isInvited{
                        self.rsvpButton.removeFromSuperview()
                        self.textView.isHidden = false
                        self.hasUserRSVP = true
                        self.rsvpIds.append((PFUser.current()?.objectId)!)
                        self.isRSVPerSubscribed.append(true)
                        self.tableView?.reloadData()
                        self.dumpVariables()
                        self.loadRSVPers()
                        
                    } else {
                        self.rsvpButton.setTitle("RSVP Pending", for: .normal)
                        self.rsvpButton.setTitleColor(UIColor.white, for: .normal)
                        self.rsvpButton.backgroundColor = UIColor.darkGray
                        self.rsvpButton.isUserInteractionEnabled = false
                        notificationMessage = "Requested access to"
                    }
                    
                    self.sendNotification(self.chatUserIds[0], message: "\(PFUser.current()!.username!) \(notificationMessage) \(self.eventTitle!)")
                    self.addEventToCalendar()
                    
                } else {
                    self.showAlert("Problem with RSVP", message: "Check internet connection and try again.")
                    self.activityIndicator.removeFromSuperview()
                    self.textView.superview?.addSubview(self.rsvpButton)
                }
            }
            
        } else {
            let prompt = UIAlertController(title: "Welcome To Hiikey!", message: "Please sign up before you RSVP", preferredStyle: .actionSheet)
            
            let signUpAction = UIAlertAction(title: "Sign Up", style: .default) {
                UIAlertAction in
                
                self.performSegue(withIdentifier: "showSignup", sender: self)
            }
            
            prompt.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            prompt.addAction(signUpAction)
            present(prompt, animated: true, completion: nil)
        }            
    }
    
    func loadBlockedUsers(){
        let blockQuery = PFQuery(className: "Block")
        blockQuery.whereKey("eventId", equalTo: objectId)
        blockQuery.whereKey("isRemoved", equalTo: false)
        blockQuery.findObjectsInBackground{
            (objects: [PFObject]?, error: Error?) -> Void in
            for object in objects!{
                let userId = object["userId"] as! String
                
                self.blockedUsers.append(userId)
            }
            print(self.blockedUsers.count)
            self.didUserRSVP()
        }        
    }
    
    func addEventToCalendar(){
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = self.eventTitle!
                event.startDate = self.rawStartDate
                event.endDate = self.rawEndDate
                event.notes = "https://www.hiikey.com/events?id=\(self.objectId) \n \(self.messages[0])"
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch _ as NSError {
                    
                }
            } else {
            }
        })
    }
    
    func dumpVariables(){
        self.userImageURL.removeAll()
        self.userName.removeAll()
        self.userDisplayName.removeAll()
    }
    
    @objc func viewImage(_ sender: UIButton){
        // 1. create SKPhoto Array from UIImage
        var images = [SKPhoto]()
        let photo = SKPhoto.photoWithImage((sender.currentBackgroundImage)!)// add some UIImage
        images.append(photo)
        
        // 2. create PhotoBrowser Instance, and present from your viewController.
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    @objc func replyAction(_ sender: UIButton){
        self.textView.text = self.replyUsername[sender.tag]
        self.textView.becomeFirstResponder()
    }
    
    @objc func getDirections(_ sender: UIButton){
        UIApplication.shared.open(URL(string: "http://maps.apple.com/?daddr=\(eventLocation.coordinate.latitude),\(eventLocation.coordinate.longitude)")!)
    }
    
    func locationCity(){
        CLGeocoder().reverseGeocodeLocation(eventLocation, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = (placemarks![0]) as CLPlacemark
                self.eventCity = pm.locality!
                
            }else {
                print("Problem with the data received from geocoder")
            }
            self.loadMessages()
        })
    }
    
    @objc func navigationActions(_ sender: UIBarButtonItem){
        if sender.tag == 0{
            self.performSegue(withIdentifier: "editEvent", sender: self)
            
        } else if sender.tag == 1 {
            removeEvent()
            
        } else if sender.tag == 2 {
            var objectsToShare = [AnyObject]()
            
            if self.eventImage != nil{
                if let myWebsite = NSURL(string: "https://www.hiikey.com/events?id=\(self.objectId!)"){
                    objectsToShare.append(myWebsite)
                    objectsToShare.append(self.eventImage)
                    
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    
                    self.present(activityVC, animated: true, completion: nil)
                }
                
            } else {
                if let myWebsite = NSURL(string: "https://www.hiikey.com/events?id=\(self.objectId!)"){
                    objectsToShare.append(myWebsite)
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    
                    self.present(activityVC, animated: true, completion: nil)
                }
            }
            
        } else if sender.tag == 3{
            reportAction()
            
        } else if sender.tag == 4{
            self.textView.text = "@everyone"
            self.textView.becomeFirstResponder()
            
        }
    }
    
    func showAlert(_ title: String, message: String){
        let prompt = UIAlertController(title: title, message: message, preferredStyle: .alert)
        prompt.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(prompt, animated: true, completion: nil)
    }
    
    func removeEvent(){
        let prompt = UIAlertController(title: "Remove \(self.eventTitle!) from Hiikey?", message: "", preferredStyle: .alert)
        
        let requestJaunt = UIAlertAction(title: "YES", style: UIAlertActionStyle.default) {
            UIAlertAction in

            let requestQuery = PFQuery(className: "Event")
            requestQuery.whereKey("objectId", equalTo: self.objectId)
            requestQuery.findObjectsInBackground{
                (objects: [PFObject]?, error: Error?) -> Void in
                if error == nil{
                    if let objects = objects as [PFObject]?{
                        //retrieve the objects from the database
                        for object in objects{
                            object["isRemoved"] = true
                            object.saveInBackground {
                                (success: Bool, error: Error?) -> Void in
                                if (success) {
                                    
                                    for user in self.rsvpIds{
                                        self.sendNotification(user, message: "\(PFUser.current()!.username!) removed \(self.eventTitle!)")
                                    }
                                    
                                    //show main storyboard
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let controller = storyboard.instantiateViewController(withIdentifier: "main") as UIViewController
                                    
                                    self.present(controller, animated: true, completion: nil)
                                    
                                } else {
                                    self.showAlert("Problem with Save", message: "Check internet connection and try again.")
                                }
                            }                            
                        }
                    }
                }
            }
        }
        
        prompt.addAction(requestJaunt)
        prompt.addAction(UIAlertAction(title: "NO", style: .default, handler: nil))
        
        present(prompt, animated: true, completion: nil)
    }
    
    func reportAction(){
        let blockPrompt = UIAlertController(title: "Report this Event? ", message: "", preferredStyle: .alert)
        blockPrompt.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        let confirmBlock = UIAlertAction(title: "Confirm", style: .default) { (action) in
            let block = PFObject(className: "Report")
            
            block["postId"] = self.objectId
            block["userId"] = PFUser.current()?.objectId
            block["isRemoved"] = false
            block["type"] = "event"
            block.saveInBackground(block: { (success: Bool, error: Error?) -> Void in
                
            })
        }
        blockPrompt.addAction(confirmBlock)
        present(blockPrompt, animated: true, completion: nil)
    }
    
    func sendNotification(_ user: String, message: String){
        let url = URL(string: "https://hiikey.herokuapp.com/notifications")
        //print(url)
        
        Alamofire.request(url!, method: .get, parameters: ["userId":user, "message": message, "eventId": objectId]).responseJSON{ response in
            print(response.request!)
            print(response.response!)
            print(response.result)
        }
    }
    
    func configureMentions(message: String, objectId: String){
        var mentions = [String]()
        
        let messageBits = message.split{$0 == " "}.map(String.init)
        for bit in messageBits{
            if bit.hasPrefix("@everyone"){
                print("everyone")
                if self.chatUserIds[0] == PFUser.current()?.objectId{
                    for user in self.rsvpIds{
                        if user != PFUser.current()?.objectId{
                            
                            let newMention = PFObject(className: "Mention")
                            newMention["eventId"] = self.objectId
                            //change to current user
                            newMention["userId"] = PFUser.current()?.objectId
                            newMention["mentionId"] = user
                            newMention["eventTitle"] = self.eventTitle!
                            newMention["message"] = message
                            newMention["messageId"] = objectId
                            newMention.saveEventually {
                                (success: Bool, error: Error?) -> Void in
                                if (success) {
                                    print("mention created")
                                    self.sendNotification(user, message: "\(PFUser.current()!.username!) in \(self.eventTitle!): \(message)")
                                }
                            }
                        }
                    }
                }
                
            } else if bit.hasPrefix("@") {
                mentions.append(bit)
                let mentionedUser = bit.split{$0 == "@"}.map(String.init)
                
                var i = 0
                for username in self.userName{
                    if username == mentionedUser[0]{
                        let newMention = PFObject(className: "Mention")
                        newMention["eventId"] = self.objectId
                        //change to current user
                        newMention["userId"] = PFUser.current()?.objectId
                        newMention["mentionId"] = self.userID[i]
                        newMention["eventTitle"] = self.eventTitle!
                        newMention["message"] = message
                        newMention["messageId"] = objectId
                        newMention.saveEventually {
                            (success: Bool, error: Error?) -> Void in
                            if (success) {
                                print("mention created")
                                self.sendNotification(self.userID[i], message: "\(PFUser.current()!.username!) in \(self.eventTitle!): \(message)")
                            }
                        }
                    }
                    i += 1
                }
            }
        }
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    @objc func exitAction(_ sender: UIButton){
        //show main storyboard
        var place = "main"
        
        if PFUser.current() == nil{
            place = "welcome"
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: place) as UIViewController
        self.present(controller, animated: true, completion: nil)
    }
    
    func createNavigation(_ isCurrentUser: Bool, isSubscribed: Bool, isConfirmed: Bool){
        let shareItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.action, target: self, action: #selector(EventPageController.navigationActions(_:)))
        shareItem.tag = 2
        
        if isCurrentUser{
            let editItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.compose, target: self, action: #selector(EventPageController.navigationActions(_:)))
            editItem.tag = 0
            
            let deleteItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(EventPageController.navigationActions(_:)))
            deleteItem.tag = 1
            
            let notifyItem = UIBarButtonItem(image: UIImage(named: "bell"), style: .plain, target: self, action: #selector(EventPageController.navigationActions(_:)))
            notifyItem.tag = 4
            
            self.navigationItem.rightBarButtonItems = [shareItem, editItem, deleteItem, notifyItem]
            
        } else if isConfirmed {
            let reportItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.reply, target: self, action: #selector(EventPageController.navigationActions(_:)))
            reportItem.tag = 3
            
            self.navigationItem.rightBarButtonItems = [reportItem,shareItem]

        } else {
            self.navigationItem.rightBarButtonItems = [shareItem]
        }
    }
}
