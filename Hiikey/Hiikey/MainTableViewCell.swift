//
//  MainTableViewCell.swift
//  Hiikey
//
//  Created by Mac Owner on 12/22/16.
//  Copyright © 2016 SocialGroupe Incorporated. All rights reserved.
//

import UIKit
import SnapKit
import ActiveLabel

class MainTableViewCell: UITableViewCell {
    
    //guest list
    @IBOutlet weak var guestImage: UIImageView!
    @IBOutlet weak var guestName: UILabel!
    @IBOutlet weak var guestUsername: UILabel!
    @IBOutlet weak var guestSwitch: UISwitch!
    
    @IBOutlet weak var guestSegment: UISegmentedControl!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var noPlansLabel: UILabel!
    
    @IBOutlet weak var noPlansExplain: UILabel!
    
    @IBOutlet weak var instagramButton: UIButton!
    
    @IBOutlet weak var facebookButton: UIButton!
    
    @IBOutlet weak var twitterButton: UIButton!
    
    @IBOutlet weak var noContactsLabel: UITextView!
    @IBOutlet weak var settingsButton: UIButton!
    
    //activity

    
    lazy var activityUsername: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var activityMessage: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 16)
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var activityImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = UIColor.black
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 25
        image.clipsToBounds = true
        return image
    }()
    
    lazy var activityAccept: UIButton = {
        let button = UIButton()
        button.setTitle("Accept", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0)
        button.contentHorizontalAlignment = .center
        button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 18)
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        return button
    }()
    
    lazy var activityCreatedAt: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 16)
        label.textColor = UIColor.darkGray
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    //schedule **********************
    lazy var profileName: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        return label
    }()
    
    lazy var profileUsername: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 16)
        label.textColor = UIColor.lightGray
        label.numberOfLines = 0
        return label
    }()
    
    lazy var profileImage: UIImageView = {
        let image = UIImageView(frame: CGRect(x: 10, y: 0, width: 100, height: 100))
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        image.image = UIImage(named: "appy")
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = image.frame.width / 2
        image.clipsToBounds = true
        return image
    }()
    //
    lazy var eventTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        return label
    }()
    
    lazy var eventDate: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 15)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var eventImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 5
        image.clipsToBounds = true
        return image
    }()
    
    lazy var eventDescription: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 15)
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    //eventpage ************************
    
    //exit cell
    //only if coming from link
    lazy var exitButton: UIButton = {
        let image = UIButton()
        image.setTitle("Exit", for: .normal)
        image.contentHorizontalAlignment = .left
        image.setTitleColor( UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0), for: .normal)
        image.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 24)
        return image 
    }()
    
    //mark - event info
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 28)
        label.numberOfLines = 2
        return label
    }()
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.numberOfLines = 2
        return label
    }()
    
    lazy var flyerButton: UIButton = {
        let image = UIButton()
        image.layer.cornerRadius = 5
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        return image
    }()

    lazy var addressButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "direction"), for: .normal)
        button.setTitle("Loading", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.contentHorizontalAlignment = .left
        button.isUserInteractionEnabled = false
        button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 18)
        button.titleLabel?.numberOfLines = 2
        return button
    }()
    
    lazy var rsvpCount: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "people"), for: .normal)
        button.setTitle("Loading", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.contentHorizontalAlignment = .left
        button.isUserInteractionEnabled = false
        button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 18)
        return button
    }()
    
    lazy var hostUsername: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 15)
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        return label
    }()
    
    lazy var hostName: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        return label
    }()
    
    lazy var hostDescription: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 16)
        label.numberOfLines = 0
        return label
    }()
    
    lazy var hostImage: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 25
        image.clipsToBounds = true
        image.image = UIImage(named: "appy")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    //mark - chat / rsvps *******************************
    
    lazy var countLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 18)
        label.textColor = UIColor.lightGray 
        label.textAlignment = .center
        return label
    }()
    
    lazy var createdAt: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 15)
        label.textColor = UIColor.lightGray
        label.textAlignment = .left
        label.numberOfLines = 2
        return label
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        return label
    }()
    
    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 15)
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        return label
    }()
    
   /* lazy var bodyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica Neue", size: 16)
        label.numberOfLines = 0
        return label
    }()*/
    
    lazy var bodyLabel: ActiveLabel = {
        let label = ActiveLabel()
        label.enabledTypes = [.mention, .url]
        label.font = UIFont(name: "Helvetica Neue", size: 16)
        label.numberOfLines = 0
        label.mentionColor = UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0)
        //label.mentionColor = UIColor(red: 238/255, green: 213/255, blue: 24/255, alpha: 1)
        label.URLColor = UIColor(red: 24/255, green: 120/255, blue: 238/255, alpha: 1)

        return label
    }()
    
    lazy var replyButton: UIButton = {
        let button = UIButton()
        //button.setImage(UIImage(named: "people"), for: .normal)
        button.setTitle("Reply", for: .normal)
        button.setTitleColor(UIColor(red: 238/255.0, green: 24/255.0, blue: 72/255.0, alpha: 1.0), for: .normal)
        button.contentHorizontalAlignment = .left
        button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 14)
        
        return button
    }()
    
    lazy var hostMarker: UILabel = {
        let label = UILabel()
        label.text = "H"
        label.textColor = UIColor.white
        label.backgroundColor = UIColor(red: 24/255.0, green: 238/255.0, blue: 142/255.0, alpha: 1.0)
        label.layer.cornerRadius = 7.5
        label.clipsToBounds = true
        label.textAlignment = .center
        label.isHidden = true
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 8)
        return label
    }()
    
    lazy var userImage: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 25
        image.clipsToBounds = true
        image.image = UIImage(named: "appy")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        //event page
        if reuseIdentifier == "EventInfoCell"{
            configureInfoSubviews()
            
        } else if reuseIdentifier == "MessageTableViewCell" {
            configureMessageSubviews(showBody: true)
            
        } else if reuseIdentifier == "EventInfoCell" {
            configureRSVPSubviews()
            
        } else if reuseIdentifier == "RSVPCell"{
            configureRSVPSubviews()
           // configureMessageSubviews(showBody: false)
            
            //schedule page
        }else if reuseIdentifier == "ProfileCell"{
            configureProfileSubviews()
            
        } else if reuseIdentifier == "EventCell"{
            configureEventSubviews()
            
        }else if reuseIdentifier == "MentionCell"{
            configureMentionSubview()
            
        }else if reuseIdentifier == "RequestCell"{
            configureRequestSubview()
            
        } else if reuseIdentifier == "exitCell"{
            configureExitSubview()
        }
    }
    
    // We won’t use this but it’s required for the class to compile
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureExitSubview(){
        self.addSubview(exitButton)
        
        exitButton.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(50)
            make.top.equalTo(self).offset(25)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            make.bottom.equalTo(self).offset(-5)
        }
    }
    
    func configureMentionSubview(){
        self.addSubview(activityImage)
        self.addSubview(activityMessage)
        self.addSubview(activityCreatedAt)
        
        activityImage.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(50)
            make.top.equalTo(activityCreatedAt.snp.bottom).offset(5)
            make.left.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-5)
        }
        
        activityMessage.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(activityCreatedAt.snp.bottom).offset(5)
            make.left.equalTo(activityImage.snp.right).offset(5)
            make.right.equalTo(self).offset(-5)
            make.bottom.equalTo(activityImage.snp.bottom)
        }
        
        activityCreatedAt.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(5)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-5)
            //make.bottom.equalTo(activityImage.snp.bottom)
        }
    }
    
    func configureRequestSubview(){
        self.addSubview(activityImage)
        self.addSubview(activityMessage)
        self.addSubview(activityAccept)
        
        activityImage.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(50)
            make.top.equalTo(activityMessage).offset(5)
            make.left.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-5)
        }
        
        activityMessage.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(5)
            make.left.equalTo(activityImage.snp.right).offset(5)
            make.bottom.equalTo(activityImage.snp.bottom)
        }
        
        activityAccept.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(35)
            make.width.equalTo(100)
            make.top.equalTo(activityMessage).offset(10)
            make.left.equalTo(activityMessage.snp.right).offset(2)
            make.right.equalTo(self).offset(-7)
        }
    }
    
    //schedule page
    func configureProfileSubviews(){
        self.addSubview(self.profileImage)
        self.addSubview(self.profileName)
        self.addSubview(self.profileUsername)
        
        profileImage.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(100)
            make.top.equalTo(self).offset(25)
            make.left.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-25)
        }
        
        profileName.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(profileImage).offset(25)
            make.left.equalTo(profileImage.snp.right).offset(10)
            make.right.equalTo(self).offset(-10)
        }
        
        profileUsername.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(profileName.snp.bottom).offset(2)
            make.left.equalTo(profileImage.snp.right).offset(10)
            make.right.equalTo(self).offset(-10)
        }
    }
    
    func configureEventSubviews(){
        self.addSubview(self.eventTitle)
        self.addSubview(self.eventDate)
        self.addSubview(self.eventImage)
        self.addSubview(self.eventDescription)
        
        eventTitle.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
        }
        
        eventDate.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(eventTitle.snp.bottom).offset(2)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            make.bottom.equalTo(eventImage.snp.top).offset(-10)
        }
        
        eventImage.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(200)
            make.width.equalTo(150)
            make.top.equalTo(eventDate.snp.bottom).offset(10)
            make.left.equalTo(self).offset(10)
            make.bottom.equalTo(self).offset(-10)
        }
        
        eventDescription.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(eventImage).offset(1)
            make.left.equalTo(eventImage.snp.right).offset(5)
            make.right.equalTo(self).offset(-5)
            make.bottom.equalTo(self).offset(-10)
        }
    }
    
    //event page
    func configureInfoSubviews(){
        self.addSubview(self.flyerButton)
        self.addSubview(self.titleLabel)
        self.addSubview(self.dateLabel)
        self.addSubview(self.addressButton)
        //self.addSubview(self.rsvpCount)
        self.addSubview(self.hostName)
        self.addSubview(self.hostUsername)
        self.addSubview(self.hostDescription)
        self.addSubview(self.hostImage)
        
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-20)
        }
        
        dateLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(6)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
        }
        
        flyerButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(200)
            make.width.equalTo(150)
            make.top.equalTo(dateLabel.snp.bottom).offset(5)
            make.left.equalTo(self).offset(10)
            //make.right.equalTo(self).offset(-10)
        }
        
        addressButton.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(flyerButton.snp.bottom).offset(20)
            make.left.equalTo(self).offset(5)
            make.right.equalTo(self).offset(-10)
            make.bottom.equalTo(self).offset(-20)
        }
    }
    
    func configureMessageSubviews(showBody: Bool) {
       
        self.addSubview(self.userImage)
        self.addSubview(self.nameLabel)
        //self.addSubview(self.usernameLabel)
        self.addSubview(self.bodyLabel)
        self.addSubview(self.replyButton)
        self.addSubview(self.createdAt)
        self.addSubview(self.hostMarker)
        
        //self.userImage.addSubview(self.hostMarker)
        
        createdAt.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(5)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-20)
        }
        
        userImage.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(50)
            make.top.equalTo(createdAt.snp.bottom).offset(20)
            make.left.equalTo(self).offset(10)
        }
        
        hostMarker.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(15)
            make.top.equalTo(userImage.snp.top)
            make.left.equalTo(userImage.snp.left)
        }
        
        nameLabel.snp.makeConstraints { (make) -> Void in
           /* if !showBody{
                make.top.equalTo(userImage).offset(14)

            } else {
                make.top.equalTo(userImage).offset(1)
            }*/
            make.top.equalTo(userImage).offset(1)
            make.left.equalTo(userImage.snp.right).offset(5)
            make.right.equalTo(self).offset(-20)
        }
        /*usernameLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(nameLabel.snp.bottom)
            make.left.equalTo(userImage.snp.right).offset(5)
            make.right.equalTo(self).offset(-5)
        }*/
        
        bodyLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(nameLabel.snp.bottom)
            make.left.equalTo(userImage.snp.right).offset(5)
            make.right.equalTo(self).offset(-20)
            //make.bottom.equalTo(self).offset(-20)
        }
        
        replyButton.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(bodyLabel.snp.bottom).offset(10)
            make.left.equalTo(userImage.snp.right).offset(5)
            make.right.equalTo(self).offset(-20)
            make.bottom.equalTo(self).offset(-10)
        }
        
    }
    
    func configureRSVPSubviews(){
        self.addSubview(self.userImage)
        self.addSubview(self.nameLabel)
        self.addSubview(self.usernameLabel)
        
        userImage.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(50)
            make.top.equalTo(self).offset(20)
            make.left.equalTo(self).offset(10)
        }
        
        nameLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(userImage).offset(1)
            make.left.equalTo(userImage.snp.right).offset(5)
            make.right.equalTo(self).offset(-5)
        }
        
        usernameLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(nameLabel.snp.bottom)
            make.left.equalTo(userImage.snp.right).offset(5)
            make.right.equalTo(self).offset(-5)
            make.bottom.equalTo(self).offset(-20)
        }
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
